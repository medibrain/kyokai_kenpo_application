﻿Imports System.Text.RegularExpressions

Public Class frmOpe
    Dim clsex As New clsExcel
    Dim xmlDs As New DataSet

    Dim strKimitusei As String

    Dim dtExcel As New DataTable
    Dim strmempath As String
    Dim fb As New FolderBrowserDialog

    Dim cntXLS As Integer = 0 'エクセルカウンタ
    Dim cntDOC As Integer = 0 'ワードカウンタ
    Dim cntPDF As Integer = 0 'PDFカウンタ

    Dim dtItiran As New DataTable
    Dim danpg As New Npgsql.NpgsqlDataAdapter

    Dim strfld As String

    Dim dtBeforeFiles As New DataTable '分ける前のファイルパス
    Dim dsFile As New DataSet '翻訳直後のファイル名

    Dim strSelectSheet As String = String.Empty

    Dim lstFileHeaderSettingErr As New List(Of String)
    Dim lstFileHeaderSettingWord As New List(Of String)
    Dim lstFileHeaderSettingExcel As New List(Of String)

    Dim flgCmp1 As Boolean
    Dim flgCmp2 As Boolean

#Region "オブジェクトイベント"

#Region "エクセルの管理表をロード"

    Private Sub btnExcelSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcelSelect.Click


        Dim arrsheet As New ArrayList

        'If rb_koban.Checked = False And _
        '    rb_set.Checked = False And _
        '    rb_manage.Checked = False Then
        '    MessageBox.Show("項番かセット番号かどちらかを選択してください", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    Exit Sub

        'End If

        arrsheet = clsex.LoadExcelSheet(lblManageFile.Text)
        lstSheet.DataSource = Nothing

        lstSheet.DataSource = arrsheet

        If rb_FileCount.Checked Or rb_set.Checked Or rb_FileCountSet.Checked Then
            For r As Integer = 0 To lstSheet.Items.Count - 1
                If lstSheet.Items(r).ToString = "作業表" Then
                    lstSheet.SelectedIndex = r
                    numHeaderRow.Value = 1
                    numDataRow.Value = 2
                    strSelectSheet = "作業表"
                    Exit For
                End If
                If r = lstSheet.Items.Count - 1 Then MsgBox("作業表がありません", MsgBoxStyle.Critical)
            Next

        ElseIf rb_koban.Checked Or rb_createSagyo.Checked Then
            For r As Integer = 0 To lstSheet.Items.Count - 1
                If lstSheet.Items(r).ToString Like "*別紙２－１*" Then
                    lstSheet.SelectedIndex = r
                    numHeaderRow.Value = 5
                    numDataRow.Value = 6
                    Exit For
                End If
                If r = lstSheet.Items.Count - 1 Then MsgBox("別紙２－１がありません", MsgBoxStyle.Critical)
            Next

        End If

        If Not Write2Xml("path", "manageFile", lblManageFile.Text) Then Exit Sub

    End Sub
#End Region

#Region "excelデータ取得"

    Private Sub btnGetData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetData.Click

        Try

            sensitive(False)
            dtExcel = clsex.OpenWorkBook(lstSheet.SelectedItem, numHeaderRow.Value, numDataRow.Value)

            If IsNothing(dtExcel) Then
                MsgBox("エクセルを選択してください", MsgBoxStyle.Exclamation)
                Exit Sub
            End If


            sensitive(True)


            Select Case True
                Case rb_koban.Checked, rb_FileCount.Checked, rb_createSagyo.Checked, _
                    rb_NohinDtUPD.Checked, rb_printkoban.Checked

                    strfld = "項番"

                Case rb_set.Checked, rb_FileCountSet.Checked

                    strfld = "セット番号"

            End Select

            If Not dtExcel.Columns.Contains(strfld) Then
                MessageBox.Show("excelに「" & strfld & "」が存在しません。開くファイルがあっているか確認してください。" & vbCrLf & _
                                "開くファイルが正しい場合、「" & strfld & "」が別の名前になっている可能性がありますので、別の名前になっている場合は「" & strfld & "」に直して" & vbCrLf & _
                                "再度①からやり直してください", Application.ProductName, _
                                MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show("データ取得終了", Application.ProductName, MessageBoxButtons.OK, _
                                MessageBoxIcon.Information)


            End If

            lblCnt.Text = dtExcel.Rows.Count & "件取得"
            'btnFolderPlace.Enabled = True

        Catch ex As Exception
            MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, _
                   MessageBoxIcon.Exclamation)

        Finally
            clsex.CloseExcel()
            sensitive(True)
        End Try

    End Sub
#End Region

#Region "フォルダ作成場所選択"

    Private Sub btnFolderPlace_Click(ByVal sender As System.Object, _
                                     ByVal e As System.EventArgs) _
                                     Handles btnFolderPlace.Click

        If lblFolderCreatePath.Text <> String.Empty Then fb.SelectedPath = lblFolderCreatePath.Text

        If String.IsNullOrEmpty(clsex.ofd.FileName) Then
            fb.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
        Else
            fb.SelectedPath = IO.Path.GetDirectoryName(clsex.ofd.FileName)
            '選択したフォルダまでスクロールさせる
        End If
        SendKeys.Send("{TAB}{TAB}{RIGHT}")


        fb.ShowDialog()
        lblFolderCreatePath.Text = fb.SelectedPath
        strmempath = fb.SelectedPath
        'btnRecipt.Enabled = True

        If Not Write2Xml("path", "createFolder", lblFolderCreatePath.Text) Then Exit Sub
    End Sub
#End Region

#Region "ファイル移動ボタン"

    Private Sub btnCreateFolder_FileMove_Click(ByVal sender As System.Object, _
                                               ByVal e As System.EventArgs) _
                                               Handles btnCreateFolder_FileMove.Click

        Dim strParentPath As String = String.Empty
        Dim strParentPathWaketa As String = String.Empty

        fb.SelectedPath = IO.Path.GetDirectoryName(clsex.ofd.FileName)

        cntXLS = 0
        cntDOC = 0
        cntPDF = 0

        If lblReceiptFilesPath.Text = String.Empty Then
            MessageBox.Show("レセファイルの場所を指定してください", _
                            Application.ProductName, MessageBoxButtons.OK, _
                            MessageBoxIcon.Exclamation)
            Exit Sub
        End If


        '翻訳直後のフォルダ名から、誰が作成したかを登録しておく

        If Not HonyakuFolderSeiton() Then Exit Sub

        'Gitテストのためのダミーコメント(20190301豊嶋)

        'セット番号の場合は委託区分分岐を入れる
        Dim strFolderKbn As String = String.Empty
        Dim itaku As Integer = 0
        If rb_set.Checked Then
            Select Case True
                Case rb_itaku1.Checked
                    itaku = 1
                    strFolderKbn = "翻訳要"
                Case rb_itaku2.Checked
                    itaku = 2
                    strFolderKbn = "翻訳不要"
                Case Else
                    MessageBox.Show("委託区分を選択してください", _
                                    Application.ProductName, MessageBoxButtons.OK, _
                                    MessageBoxIcon.Exclamation)
                    Exit Sub
            End Select
        End If

        strParentPath = Me.lblFolderCreatePath.Text
        strParentPathWaketa = strParentPath & "\" & _
                            strFolderKbn & strfld & "フォルダ分け済み"


        '//20170626115403 furukawa st ////////////////////////
        '//ファイル名を半角にする
        RenameHankaku()
        '//20170626115403 furukawa ed ////////////////////////

        '//20170711102540 furukawa st ////////////////////////
        '//セット番号フォルダの場合、ファイル名をハイフン付きに変換バッチをコピーして実行
        If rb_set.Checked Then
            IO.File.Copy(Application.StartupPath & "\file_hyphon.bat", _
                         lblReceiptFilesPath.Text & "\file_hyphon.bat", True)
            Dim p As Process = Process.Start(lblReceiptFilesPath.Text & "\file_hyphon.bat")
            p.WaitForExit()
        End If
        '//20170711102540 furukawa ed ////////////////////////




        For r As Integer = 0 To dtExcel.Rows.Count - 1

            '//20170626112211 furukawa st ////////////////////////
            '//医科歯科フォルダわけない

            'Select Case dtExcel.Rows(r)("種別")
            '    Case "A", "B"
            '        strParentPath = strParentPathIka

            '    Case "C"
            '        strParentPath = strParentPathShika
            '    Case Else

            'End Select

            'IO.Directory.CreateDirectory(strParentPath & "\" & _
            '         StrConv(dt.Rows(r)("項番").ToString, VbStrConv.Narrow))

            strParentPath = strParentPathWaketa


            '//20170626112211 furukawa ed ////////////////////////




            '//20170707105031 furukawa st ////////////////////////
            '//項番フォルダ名・ファイル名をハイフン付きにする

            Dim strFolderNameHyphon As String = String.Empty

            strFolderNameHyphon = AddHyphon(dtExcel.Rows(r)("項番").ToString)

            Dim strFolderName As String = _
            StrConv(AddHyphon(dtExcel.Rows(r)(strfld).ToString), VbStrConv.Narrow)


            'セット番号の場合、種別＋件数も付ける
            If rb_set.Checked Then

                'セット番号と委託区分でフィルタを掛けて取得
                Dim strfilter As String = _
                "セット番号='" & dtExcel.Rows(r)("セット番号").ToString & "'" & _
                " and 委託区分='" & itaku & "'"

                Dim cntA As Integer = 0, cntB As Integer = 0, cntC As Integer = 0

                Dim strcnt As String = String.Empty

                Dim dr() As DataRow = dtExcel.Select(strfilter)

                For r2 As Integer = 0 To dr.Length - 1

                    Select Case dr(r2)("種別").ToString
                        Case "A" : cntA += 1
                        Case "B" : cntB += 1
                        Case "C" : cntC += 1
                        Case Else

                    End Select
                Next

                If cntA > 0 Then strcnt &= "_Ax" & cntA
                If cntB > 0 Then strcnt &= "_Bx" & cntB
                If cntC > 0 Then strcnt &= "_Cx" & cntC

                strFolderName &= strcnt
            End If


            'ファイル格納先
            strParentPath = strParentPath & "\" & strFolderName

            'strParentPath = strParentPath & "\" & _
            '             StrConv(strFolderNameHyphon, VbStrConv.Narrow)

            'ファイル移動
            MoveFiles(StrConv(strFolderNameHyphon, VbStrConv.Narrow), strParentPath)

            'strParentPath = strParentPath & "\" & _
            '             StrConv(dtExcel.Rows(r)("項番").ToString, VbStrConv.Narrow)

            'MoveFiles(StrConv(dtExcel.Rows(r)("項番").ToString, VbStrConv.Narrow), strParentPath)

            '//20170707105031 furukawa ed ////////////////////////


        Next


        'フォルダ情報を更新
        If Not UpdateSettingFolderTable() Then Exit Sub
        '作業表エクセルの更新
        If Not UpdateSagyohyo() Then Exit Sub

        MessageBox.Show("移動終了", Application.ProductName, _
                        MessageBoxButtons.OK, MessageBoxIcon.Information)
        '結果表示
        ShowResult()


    End Sub
#End Region

#Region "レセデータ場所選択"

    Private Sub btnRecipt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRecipt.Click

        If Not IsNothing(strmempath) Then strmempath = IO.Path.GetDirectoryName(lblManageFile.Text)
        If lblReceiptFilesPath.Text <> String.Empty Then strmempath = lblReceiptFilesPath.Text

        If Not System.IO.Directory.Exists(strmempath) Then
            If Not IO.Directory.Exists(IO.Path.GetDirectoryName(lblManageFile.Text)) Then
                strmempath = Environment.SpecialFolder.Desktop
            Else
                strmempath = IO.Path.GetDirectoryName(lblManageFile.Text)
            End If

        End If

        fb.SelectedPath = strmempath
        '選択したフォルダまでスクロールさせる
        SendKeys.Send("{TAB}{TAB}{RIGHT}")

        fb.ShowDialog()
        Me.lblReceiptFilesPath.Text = fb.SelectedPath
        ' btnCountFiles.Enabled = True

        If Not Write2Xml("path", "rezeptFolder", Me.lblReceiptFilesPath.Text) Then Exit Sub

    End Sub
#End Region

#Region "フォームクローズ"

    Private Sub frmOpe_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        clsex.CloseExcel()

    End Sub

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        clsex.CloseExcel()

    End Sub
#End Region

#Region "ラジオボタン選択"
    Private Sub rb_Header_CheckedChanged(sender As Object, e As EventArgs) Handles rb_Header.CheckedChanged
        sensitive(True)
    End Sub

    Private Sub rb_FileFolderName_CheckedChanged(sender As Object, e As EventArgs) Handles rb_FileFolderName.CheckedChanged
        sensitive(True)
    End Sub
    Private Sub rb_set_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_set.CheckedChanged
        sensitive(True)
    End Sub

    Private Sub rb_koban_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_koban.CheckedChanged
        sensitive(True)
    End Sub

    Private Sub rb_manage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_FileCount.CheckedChanged
        sensitive(True)
    End Sub
    Private Sub rb_createSagyo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_createSagyo.CheckedChanged
        sensitive(True)
        GetCombo()

    End Sub
    Private Sub rbNohinDtUPD_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_NohinDtUPD.CheckedChanged
        sensitive(True)
    End Sub

    Private Sub rb_FileCountSet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rb_FileCountSet.Click
        sensitive(True)
    End Sub

    Private Sub rb_printkoban_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_printkoban.CheckedChanged
        sensitive(True)
    End Sub



#End Region

#Region "作業表ボタン"
    Private Sub btnCreateSagyo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreateSagyo.Click

        Try
            sensitive(False)
            If Not CreateSagyoList() Then Exit Sub


        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            sensitive(True)
        End Try

    End Sub
#End Region

#Region "ファイル数カウントボタン"

    Private Sub btnCountFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCountFiles.Click
        Dim arrFolders() As String
        arrFolders = IO.Directory.GetDirectories(lblReceiptFilesPath.Text)
        Dim dt As New DataTable

        Try
            sensitive(False)


            dt.TableName = "file_count"
            dt.Columns.Add("項番")
            dt.Columns.Add("セット番号")
            dt.Columns.Add("月内通番")
            dt.Columns.Add("Chkエクセル数")
            dt.Columns.Add("Chkワード数")
            dt.Columns.Add("ChkPDF数")
            dt.AcceptChanges()


            Select Case True
                Case rb_FileCountSet.Checked

                    'セットフォルダ内を検索
                    For Each fld As String In arrFolders
                        Dim dr As DataRow
                        ' dr = dt.NewRow
                        Dim foldername As String = IO.Path.GetFileName(fld)

                        If Not IsNumeric(foldername.Substring(0, 3)) Then
                            If MessageBox.Show(foldername & vbCrLf & _
                                            "セット番号フォルダは数字３桁で始まります。飛ばしますか？", _
                                            Application.ProductName, _
                                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                                Continue For
                            End If
                        End If

                        Dim folderpath As String = IO.Path.GetFullPath(fld)
                        Dim cntPDFFile As Integer = 0
                        Dim cntDOCFile As Integer = 0

                        For Each fl As Object In IO.Directory.GetFiles(fld)
                            Dim strfl As String = IO.Path.GetFileNameWithoutExtension(fl).ToString

                            Dim strkoban As String = IO.Path.GetFileNameWithoutExtension(fl).Replace("-", String.Empty)
                            dr = dt.NewRow

                            dr("項番") = strkoban
                            'If StrComp(IO.Path.GetExtension(fl), ".pdf", CompareMethod.Text) = 0 Then
                            'If IO.Path.GetExtension(fl) = ".pdf" Then dr("ChkPDF数") = "○"

                            'If StrComp(IO.Path.GetExtension(fl), ".docx", CompareMethod.Text) = 0 Then
                            'If IO.Path.GetExtension(fl) = ".docx" Then dr("Chkワード数") = "○"

                            dr("ChkPDF数") = IO.Directory.GetFiles(fld, strfl & ".pdf", IO.SearchOption.AllDirectories).Count
                            dr("Chkワード数") = IO.Directory.GetFiles(fld, strfl & ".doc", IO.SearchOption.AllDirectories).Count
                            If dr("Chkワード数") = 0 Then dr("Chkワード数") = IO.Directory.GetFiles(fld, strfl & ".docx", IO.SearchOption.AllDirectories).Count

                            dt.Rows.Add(dr)

                        Next

                    Next

                Case rb_FileCount.Checked

                    '項番フォルダ内を検索
                    For Each fld As String In arrFolders
                        Dim dr As DataRow
                        dr = dt.NewRow
                        Dim foldername As String = IO.Path.GetFileName(fld)

                        If Not foldername.Contains("-") Then
                            If MessageBox.Show(foldername & vbCrLf & _
                                            "項番フォルダにハイフンがありません。飛ばしますか？", _
                                            Application.ProductName, _
                                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                                Continue For
                            End If
                        End If

                        dr("項番") = foldername.Replace("-", String.Empty)
                        dr("月内通番") = foldername.Split("-")(1)
                        dr("Chkエクセル数") = IO.Directory.GetFiles(fld, "*.xls", IO.SearchOption.AllDirectories).Count
                        dr("Chkワード数") = IO.Directory.GetFiles(fld, "*.doc", IO.SearchOption.AllDirectories).Count
                        dr("ChkPDF数") = IO.Directory.GetFiles(fld, "*.pdf", IO.SearchOption.AllDirectories).Count

                        dt.Rows.Add(dr)

                    Next
            End Select


            Dim clsex As New ForExcel_kk.clsExcel_kk
            clsex.CreateExcelNoSave()

            If Not clsex.OpenWorkBook(Me.lblManageFile.Text, strSelectSheet) Then
                'If Not clsex.OpenWorkBook(Me.lblManageFile.Text, "作業表") Then
                MsgBox("作業表がありません")
                Exit Sub
            End If

            'If Not clsex.SetCellValue("作業表", dt) Then Exit Sub

            Dim lstErr As List(Of String) = clsex.SetCellValue(strSelectSheet, dt)
            'Dim lstErr As List(Of String) = clsex.SetCellValue("作業表", dt)

            'If lstErr.Count > 0 Then
            '    Dim dlg As New dlgErr
            '    dlg.txt.Text &= "ファイル数カウント" & vbCrLf
            '    For Each s As String In lstErr
            '        dlg.txt.Text &= s & vbCrLf
            '    Next
            '    dlg.Show()

            'End If

            If Not rb_FileCountSet.Checked Then
                If Not GetScore() Then Exit Sub
            End If


            MessageBox.Show("終了")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            clsex.CloseExcel()
            sensitive(True)

        End Try

    End Sub
#End Region

#Region "コンボ取得"

    Private Function GetCombo() As Boolean
        Dim cn As New Npgsql.NpgsqlConnection

        Try
            cn = DBConn()
            Dim da As New Npgsql.NpgsqlDataAdapter
            Dim dt As New DataTable

            Dim cmd As New Npgsql.NpgsqlCommand
            cmd.Connection = cn
            cmd.CommandText = "select 回数 from sagyohyo group by 回数 order by 回数 desc"
            da.SelectCommand = cmd
            da.Fill(dt)
            cmbKaisu.Items.Clear()
            For r As Integer = 0 To dt.Rows.Count - 1
                cmbKaisu.Items.Add(dt.Rows(r)("回数").ToString)
            Next
            dt.Clear()

            cmd.CommandText = "select 委託区分 from sagyohyo group by 委託区分"
            da.SelectCommand = cmd
            da.Fill(dt)
            cmbItaku.Items.Clear()
            For r As Integer = 0 To dt.Rows.Count - 1
                cmbItaku.Items.Add(dt.Rows(r)("委託区分").ToString)
            Next

            cn.Close()
            Return True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Return False
        Finally


        End Try

    End Function
#End Region

#Region "DB登録ボタン"
    ''' <summary>
    ''' datatableからcsvを作成し、DBにインポート
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnImport2DB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport2DB.Click
        Dim strSQL As String = String.Empty
        Dim tran As Npgsql.NpgsqlTransaction = Nothing
        Dim cn As New Npgsql.NpgsqlConnection

        Try
            cn = DBConn()

            Dim cmd As New Npgsql.NpgsqlCommand
            cmd.Connection = cn

            Dim cnt As Integer = 0
            Dim r As Integer = 0

            'エクセルを一旦CSVにする→なんでやった？→ヘッダ文字列が改行されてるためヘッダを削除した
            Dim sw As New IO.StreamWriter("一覧表.csv", False, System.Text.Encoding.GetEncoding("utf-8"))
            Dim strWrite As String = String.Empty
            For r = 0 To dtExcel.Rows.Count - 1

                strWrite = String.Empty
                For c As Integer = 0 To dtExcel.Columns.Count - 2
                    strWrite &= dtExcel.Rows(r)(c).ToString & ","
                Next
                strWrite = strWrite.Substring(0, strWrite.Length - 1)
                sw.WriteLine(strWrite)
            Next

            sw.Close()
            lblRecCnt.Text = "管理表：" & dtExcel.Rows.Count

            '作成したCSVをdbに取り込む
            tran = cn.BeginTransaction
            For r = 0 To dtExcel.Rows.Count - 1

                Select Case True
                    Case rb_createSagyo.Checked
                        '同じ項番がある場合は飛ばす
                        strSQL = String.Empty
                        strSQL = "select * from manage where 項番='" & dtExcel.Rows(r)("項番") & "'"
                        cmd.CommandText = strSQL
                        If cmd.ExecuteScalar() > 0 Then Continue For


                        strSQL = String.Empty


                        strSQL = "insert into manage values("

                        For c As Integer = 0 To dtExcel.Columns.Count - 2
                            If dtExcel.Rows(r)(c).ToString = String.Empty Then
                                strSQL &= "null,"
                            Else
                                strSQL &= "'" & dtExcel.Rows(r)(c).ToString & "',"
                            End If
                        Next
                        strSQL = strSQL.Substring(0, strSQL.Length - 1)
                        strSQL &= ")"


                    Case rb_NohinDtUPD.Checked
                        '項番がない場合は飛ばす
                        strSQL = String.Empty
                        strSQL = "select * from manage where 項番='" & dtExcel.Rows(r)("項番") & "'"
                        cmd.CommandText = strSQL
                        If cmd.ExecuteScalar() = 0 Then Continue For

                        '納品日がからの場合抜ける
                        If dtExcel.Rows(r)("納品日").ToString = String.Empty Then Continue For

                        strSQL = String.Empty

                        strSQL = "update manage set 納品日='" & dtExcel.Rows(r)("納品日").ToString & "' "
                        strSQL &= "where (項番='" & dtExcel.Rows(r)("項番").ToString & "' and trim(納品日) is null) or " & _
                                "(項番='" & dtExcel.Rows(r)("項番").ToString & "' and trim(納品日)='')"

                End Select


                cmd.CommandText = strSQL
                cmd.ExecuteScalar()
                cnt += 1
            Next

            tran.Commit()
            MessageBox.Show("管理表取込完了", Application.ProductName, MessageBoxButtons.OK)
            lblRecCnt.Text &= " 登録：" & cnt


        Catch npgex As Npgsql.NpgsqlException

            MessageBox.Show("btnImport2DB" & vbCrLf & strSQL & vbCrLf & npgex.Message)
            tran.Rollback()

        Catch ex As Exception
            MessageBox.Show(strSQL & vbCrLf & ex.Message)
            tran.Rollback()
        Finally
            GetCombo()

            cn.Close()

        End Try

    End Sub
#End Region

#Region "シート名のリストをクリックした時"


    Private Sub lstSheet_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lstSheet.MouseClick
        If lstSheet.Items.Count = 0 Then Exit Sub

        strSelectSheet = lstSheet.SelectedItem.ToString
    End Sub
#End Region


#Region "印刷画面"
    Private Sub btnPrt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrt.Click
        If Not IO.Directory.Exists(Me.lblReceiptFilesPath.Text) Then
            MessageBox.Show("レセプトファイルの場所を選択してください", Application.ProductName, _
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        Dim frmPrt As New frmPrt(dtExcel, Me.lblReceiptFilesPath.Text)
        frmPrt.ShowDialog()
        frmPrt.Close()



    End Sub
#End Region

#Region "ロードイベント"
    Private Sub frmOpe_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not LoadXml() Then Exit Sub
        GetVer()

        '//20180720100557 furukawa st ////////////////////////
        '//最初はチェック入れない
        sensitive(False)
        gb.Enabled = True
        '//20180720100557 furukawa ed ////////////////////////

    End Sub
#End Region

#Region "作業表出力"
    Private Sub btnSagyolistOut_Click(sender As Object, e As EventArgs) Handles btnSagyolistOut.Click
        '作業表シート
        If Not CreateSagyoListExcel() Then Exit Sub
        'セット委託区分シート
        If Not CreateSetItakuExcel() Then Exit Sub

        MessageBox.Show("作業表出力終了", Application.ProductName, MessageBoxButtons.OK)

    End Sub
#End Region


 

#Region "ファイルヘッダ変更"

#Region "ヘッダ設定ボタン"

    Private Sub btnHeader_Click(sender As Object, e As EventArgs) Handles btnHeader.Click

        sensitive(False)

        pb1.Value = 0
        lstFileHeaderSettingErr.Clear()

        If Not IO.Directory.Exists(lblReceiptFilesPath.Text) Then
            MessageBox.Show("レセプトフォルダが見つかりません。再設定してください", Application.ProductName, _
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            sensitive(True)
            Exit Sub
        End If

        'ファイル名取得
        For Each strdir As String In IO.Directory.GetDirectories(lblReceiptFilesPath.Text)
            For Each strfn As String In IO.Directory.GetFiles(strdir)
                If IO.Path.GetExtension(strfn) = ".docx" Then lstFileHeaderSettingWord.Add(strfn)
                If IO.Path.GetExtension(strfn) = ".xlsx" Then lstFileHeaderSettingExcel.Add(strfn)
            Next
        Next

        'Word,Excelの非同期タスクを走らせる
        If chkDoc.Checked Then bg1.RunWorkerAsync()
        If chkXls.Checked Then bg2.RunWorkerAsync()

        If lstFileHeaderSettingErr.Count > 0 Then
            Dim strerr As String = String.Empty

            For Each s As String In lstFileHeaderSettingErr
                strerr &= s & vbCrLf
            Next
            MsgBox(strerr)
        End If

    End Sub
#End Region

#Region "Wordヘッダ文字列設定"
    Private Function WordHeader(ByVal strFn As String) As Boolean


        Dim psi As New ProcessStartInfo("WScript.exe")
        psi.Arguments = "vbsWordHeaderForKK.vbs """ & strFn & "," & strKimitusei & """"

        Dim job As Process = Process.Start(psi)
        job.WaitForExit()

        'エラーがあったらエラーファイルリストに追加
        Dim ret As String = job.ExitCode.ToString
        If ret <> "0" Then lstFileHeaderSettingErr.Add(ret)

    End Function
#End Region

#Region "Excelヘッダ文字列設定"
    Private Function ExcelHeader(ByVal strFN As String) As Boolean
        Dim psi As New ProcessStartInfo("WScript.exe")
        psi.Arguments = "vbsExcelHeaderForKK.vbs """ & strFN & "," & strKimitusei & """"

        getPinfo(strFN)

        Dim job As Process = Process.Start(psi)
        job.WaitForExit()

        'エラーがあったらエラーファイルリストに追加
        Dim ret As String = job.ExitCode.ToString
        If ret <> "0" Then lstFileHeaderSettingErr.Add(ret)
    End Function
#End Region

    Dim lst As New List(Of String)

    '//20181116173506 furukawa st ////////////////////////
    '//別紙２から個人情報を取得しdb取込用ファイル作る
    Private Sub getPinfo(strPath As String)
        Dim wb As New ClosedXML.Excel.XLWorkbook(strPath)
        Dim ws As ClosedXML.Excel.IXLWorksheet = Nothing

        Try

            If wb.Worksheets.Count = 1 Then Exit Sub

            'ws = wb.Worksheets(2).Worksheet
            wb.TryGetWorksheet("別紙2", ws)

            '//20181116173607 furukawa st ////////////////////////
            '//別紙２がない場合抜ける
            If IsNothing(ws) Then Exit Sub
            '//20181116173607 furukawa ed ////////////////////////

            Dim strpi As String = String.Empty

            strpi = """" & ws.Cell("j2").Value.ToString() & """,""" &
                ws.Cell("c9").Value.ToString() & """,""" & ws.Cell("c10").Value.ToString() & """,""" &
                ws.Cell("e10").Value.ToString() & """,""" & ws.Cell("c12").Value.ToString() & """"

            ' wb.Dispose()
            If Not lst.Contains(strpi) Then lst.Add(strpi)


        Catch ex As Exception
            MessageBox.Show(strPath & vbCrLf & ex.Message,
                            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            wb.Dispose()

        End Try
    End Sub

#Region "bg1"

    Private Sub bg_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bg1.DoWork

        'wordファイルを一つずつスクリプトにわたす
        For r As Integer = 0 To lstFileHeaderSettingWord.Count - 1
            'ヘッダ編集用スクリプト
            WordHeader(lstFileHeaderSettingWord(r))
            bg1.ReportProgress(r * 100 / lstFileHeaderSettingWord.Count)
        Next

        e.Result = True

    End Sub

    Private Sub bg_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bg1.ProgressChanged
        pb1.Value = e.ProgressPercentage

    End Sub

    Private Sub bg_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bg1.RunWorkerCompleted

        pb1.Value = 100

        flgCmp1 = True

        If flgCmp2 Or Not chkXls.Checked Then
            sensitive(True)
            If lstFileHeaderSettingErr.Count > 0 Then
                Dim frm As New frmList
                frm.dg.DataSource = lstFileHeaderSettingErr
                frm.Show()
            End If
        End If

    End Sub
#End Region

#Region "bg2"

    Private Sub bg2_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bg2.DoWork

        'エクセルを一つずつスクリプトにわたす
        For r As Integer = 0 To lstFileHeaderSettingExcel.Count - 1
            'ヘッダ・フォーマット編集用スクリプト
            ExcelHeader(lstFileHeaderSettingExcel(r))
            bg2.ReportProgress(r * 100 / lstFileHeaderSettingExcel.Count)
        Next

        e.Result = True

    End Sub

    Private Sub bg2_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bg2.ProgressChanged
        pb2.Value = e.ProgressPercentage
    End Sub

    Private Sub bg2_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bg2.RunWorkerCompleted
        pb2.Value = 100

        flgCmp2 = True
        If flgCmp1 Or Not chkDoc.Checked Then

            sensitive(True)
            If lstFileHeaderSettingErr.Count > 0 Then
                Dim frm As New frmList
                frm.dg.DataSource = lstFileHeaderSettingErr
                frm.Show()
            End If

            '//20180828174143 furukawa st ////////////////////////
            '//個人情報の一覧csv作成（KK取込用）

            Dim sw As New System.IO.StreamWriter(Application.StartupPath & "\pinfo.csv", False)
     
            For Each s As String In lst
                sw.WriteLine(s)
            Next

            sw.Close()
            '//20180828174143 furukawa ed ////////////////////////


        End If

    End Sub
#End Region


#End Region

#Region "ファイル・フォルダ名変更"

    Private Sub btnRename_Click(sender As Object, e As EventArgs) Handles btnRename.Click

        If Not RenameFolderFile(True) Then Exit Sub

        MessageBox.Show("ファイル名・パス名変更終了", Application.ProductName, _
                        MessageBoxButtons.OK, MessageBoxIcon.Information)


    End Sub

    Private Function RenameFolderFile(flg As Boolean) As Boolean

        Try
            'ファイル名取得
            For Each strdir As String In IO.Directory.GetDirectories(lblReceiptFilesPath.Text)

                'ファイル名から変更する
                For Each strfn As String In IO.Directory.GetFiles(strdir)

                    Dim fi As New IO.FileInfo(strfn)
                    'フォルダパス取得
                    Dim tmpFilePath As String = fi.FullName.Replace(fi.Name, String.Empty)
                    '切り取る長さ
                    Dim intLength As Integer = 0
                    '拡張子抜きのファイル名
                    Dim tmpFileName As String = IO.Path.GetFileNameWithoutExtension(fi.Name)
                    Dim strExt As String = IO.Path.GetExtension(fi.Name)

                    '143003-123-1A
                    '143003-123-1A-1 
                    '143003-123-1A-11 の場合はリネーム、ほかは放置
                    '143003-123-1A-1赤
                    Select Case True
                        '//20180628171625 furukawa st ////////////////////////
                        '//枝番２桁も対応
                        Case Regex.IsMatch(tmpFileName, "^\d{6}-\d{3}-\d{1}[A-C]{1}$"), _
                            Regex.IsMatch(tmpFileName, "^\d{6}-\d{3}-\d{1}[A-C]{1}-\d$"), _
                            Regex.IsMatch(tmpFileName, "^\d{6}-\d{3}-\d{1}[A-C]{1}-\d\d$")
                            '//20180628171625 furukawa ed ////////////////////////
                            tmpFileName = fi.Name
                        Case Regex.IsMatch(tmpFileName, "^\d{6}-\d{3}-\d{1}[A-C]{1}赤")
                            tmpFileName = tmpFileName.Substring(0, 13) & strExt
                        Case Else
                            Continue For

                    End Select

                    fi.MoveTo(tmpFilePath & strKimitusei & tmpFileName)
                Next

                '次にフォルダ名にしないとパスが見えなくなる
                Dim di As New IO.DirectoryInfo(strdir)
                '一つ上のパス
                Dim tmpDirParentPath As String = di.FullName.Replace(di.Name, String.Empty)

                Dim tmpDirPath As String = di.Name

                Select Case True
                    Case Regex.IsMatch(tmpDirPath, "^\d{6}-\d{3}-\d{1}[A-C]{1}$")
                        tmpDirPath = di.Name
                    Case Regex.IsMatch(tmpDirPath, "^\d{6}-\d{3}-\d{1}[A-C]")
                        tmpDirPath = tmpDirPath.Substring(0, 13)
                    Case Else
                        Continue For
                End Select


                di.MoveTo(tmpDirParentPath & strKimitusei & tmpDirPath)

            Next

            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, Application.ProductName, _
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False

        End Try

    End Function
#End Region


#Region "色付ボタン"

    Private Sub btnColor_Click(sender As Object, e As EventArgs) Handles btnColor.Click
        SetColor()
    End Sub
#End Region





#End Region

#Region "関数"

#Region "移動結果表示"

    ''' <summary>
    ''' 移動結果表示
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ShowResult()

        Dim dlgRes As New dlgRes
        Dim tmpdt As New DataTable
        tmpdt = dsFile.Tables(0).Clone
        tmpdt.PrimaryKey = Nothing

        Select Case True
            Case rb_set.Checked
                For Each dt As DataTable In dsFile.Tables
                    For Each dr In dt.Rows
                        tmpdt.ImportRow(dr)
                    Next
                Next

            Case rb_koban.Checked

        End Select
        tmpdt.AcceptChanges()

        For Each r As DataRow In tmpdt.Rows
            For c As Integer = 0 To r.ItemArray.Count - 1

                Select Case True
                    Case rb_set.Checked
                        If r("翻訳直後フォルダ").ToString <> String.Empty AndAlso _
                            r("pdf").ToString = String.Empty Then r.Item("pdf") = "×"
                        If r("翻訳直後フォルダ").ToString <> String.Empty AndAlso _
                            r("docx").ToString = String.Empty Then r.Item("docx") = "×"
                        If r("翻訳直後フォルダ").ToString <> String.Empty AndAlso _
                            r("set_pdf").ToString = String.Empty Then r.Item("set_pdf") = "×"
                        If r("翻訳直後フォルダ").ToString <> String.Empty AndAlso _
                            r("set_docx").ToString = String.Empty Then r.Item("set_docx") = "×"

                End Select

            Next
        Next

        dlgRes.dgv.DataSource = tmpdt
        dlgRes.Show()



    End Sub
#End Region

#Region "センシティブ"

    Private Sub sensitive(ByVal flg As Boolean)

        For Each ctl As Control In Me.Controls
            ctl.Enabled = flg
            If TypeOf (ctl) Is ListBox Then Continue For
            If TypeOf (ctl) Is Label Then Continue For
            Select Case True

                Case rb_Header.Checked 'ヘッダ設定の場合はヘッダボタンのみ
                    If ctl.Name <> btnHeader.Name And _
                        ctl.Name <> btnRecipt.Name And _
                        ctl.Name <> gb.Name And _
                        ctl.Name <> pnlHeader.Name Then ctl.Enabled = False

                Case rb_FileFolderName.Checked 'ファイル名・フォルダ名変更
                    If ctl.Name <> btnRename.Name And _
                        ctl.Name <> btnRecipt.Name And _
                        ctl.Name <> gb.Name And _
                        ctl.Name <> pnlFF.Name Then ctl.Enabled = False

                Case rb_NohinDtUPD.Checked '納品日更新
                    If ctl.Name = btnRecipt.Name Then ctl.Enabled = False
                    If ctl.Name = btnFolderPlace.Name Then ctl.Enabled = False
                    If ctl.Name = btnCreateFolder_FileMove.Name Then ctl.Enabled = False
                    If ctl.Name = gbItaku.Name Then ctl.Enabled = False
                    If ctl.Name = btnCountFiles.Name Then ctl.Enabled = False
                    If ctl.Name = btnCreateSagyo.Name Then ctl.Enabled = False
                    If ctl.Name = btnPrt.Name Then ctl.Enabled = False
                    If ctl.Name = btnSagyolistOut.Name Then ctl.Enabled = False
                    If ctl.Name = btnHeader.Name Then ctl.Enabled = False
                    If ctl.Name = btnRename.Name Then ctl.Enabled = False
                    If ctl.Name = pnlHeader.Name Then ctl.Enabled = False
                    If ctl.Name = pnlFF.Name Then ctl.Enabled = False
                    If ctl.Name = pnlSagyohyo.Name Then ctl.Enabled = False

                Case rb_createSagyo.Checked '作業表作成

                    If ctl.Name = btnRecipt.Name Then ctl.Enabled = False
                    If ctl.Name = btnFolderPlace.Name Then ctl.Enabled = False
                    If ctl.Name = btnCreateFolder_FileMove.Name Then ctl.Enabled = False
                    If ctl.Name = gbItaku.Name Then ctl.Enabled = False
                    If ctl.Name = btnCountFiles.Name Then ctl.Enabled = False
                    If ctl.Name = btnPrt.Name Then ctl.Enabled = False
                    If ctl.Name = btnRename.Name Then ctl.Enabled = False
                    If ctl.Name = pnlHeader.Name Then ctl.Enabled = False
                    If ctl.Name = pnlFF.Name Then ctl.Enabled = False


                Case rb_FileCount.Checked, rb_FileCountSet.Checked 'ファイル数カウント
                    If ctl.Name = btnCreateFolder_FileMove.Name Then ctl.Enabled = False
                    If ctl.Name = btnImport2DB.Name Then ctl.Enabled = False
                    If ctl.Name = btnCreateSagyo.Name Then ctl.Enabled = False
                    If ctl.Name = btnPrt.Name Then ctl.Enabled = False
                    If ctl.Name = btnSagyolistOut.Name Then ctl.Enabled = False
                    If ctl.Name = btnRename.Name Then ctl.Enabled = False
                    If ctl.Name = pnlHeader.Name Then ctl.Enabled = False
                    If ctl.Name = pnlFF.Name Then ctl.Enabled = False
                    If ctl.Name = btnFolderPlace.Name Then ctl.Enabled = False
                    If ctl.Name = pnlSagyohyo.Name Then ctl.Enabled = False

                Case rb_set.Checked 'セット番号
                    If ctl.Name = btnCountFiles.Name Then ctl.Enabled = False
                    If ctl.Name = btnImport2DB.Name Then ctl.Enabled = False
                    If ctl.Name = btnCreateSagyo.Name Then ctl.Enabled = False
                    If ctl.Name = btnCountFiles.Name Then ctl.Enabled = False
                    If ctl.Name = btnPrt.Name Then ctl.Enabled = False
                    If ctl.Name = btnSagyolistOut.Name Then ctl.Enabled = False
                    If ctl.Name = btnRename.Name Then ctl.Enabled = False
                    If ctl.Name = pnlHeader.Name Then ctl.Enabled = False
                    If ctl.Name = pnlFF.Name Then ctl.Enabled = False
                    If ctl.Name = pnlSagyohyo.Name Then ctl.Enabled = False

                Case rb_koban.Checked '項番フォルダ
                    If ctl.Name = gbItaku.Name Then ctl.Enabled = False
                    If ctl.Name = btnImport2DB.Name Then ctl.Enabled = False
                    If ctl.Name = btnCreateSagyo.Name Then ctl.Enabled = False
                    If ctl.Name = btnCountFiles.Name Then ctl.Enabled = False
                    If ctl.Name = btnPrt.Name Then ctl.Enabled = False
                    If ctl.Name = btnSagyolistOut.Name Then ctl.Enabled = False
                    If ctl.Name = btnRename.Name Then ctl.Enabled = False
                    If ctl.Name = btnHeader.Name Then ctl.Enabled = False
                    If ctl.Name = pnlHeader.Name Then ctl.Enabled = False
                    If ctl.Name = pnlFF.Name Then ctl.Enabled = False
                    If ctl.Name = pnlSagyohyo.Name Then ctl.Enabled = False

                Case rb_printkoban.Checked '項番フォルダ印刷
                    If ctl.Name = gbItaku.Name Then ctl.Enabled = False
                    If ctl.Name = btnImport2DB.Name Then ctl.Enabled = False
                    If ctl.Name = btnCreateSagyo.Name Then ctl.Enabled = False
                    If ctl.Name = btnCountFiles.Name Then ctl.Enabled = False
                    If ctl.Name = btnCreateFolder_FileMove.Name Then ctl.Enabled = False
                    If ctl.Name = btnFolderPlace.Name Then ctl.Enabled = False
                    If ctl.Name = btnSagyolistOut.Name Then ctl.Enabled = False
                    If ctl.Name = btnRename.Name Then ctl.Enabled = False
                    If ctl.Name = pnlHeader.Name Then ctl.Enabled = False
                    If ctl.Name = pnlFF.Name Then ctl.Enabled = False

                    If ctl.Name = pnlSagyohyo.Name Then ctl.Enabled = False
            End Select


        Next

    End Sub
#End Region

#Region "項番をハイフン付きに変換"
    '//20170707104907 furukawa  ////////////////////////
    ''' <summary>
    ''' 項番をハイフン付きに変換
    ''' </summary>
    ''' <param name="strChange"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function AddHyphon(ByVal strChange As String) As String
        If strChange.Length < 10 Then Return strChange

        strChange = strChange.Substring(0, 6) & "-" & _
                    strChange.Substring(6, 3) & "-" & _
                    strChange.Substring(9)
        Return strChange
    End Function
#End Region

#Region "ファイル名半角に"
    '//20170626115403 furukawa st ////////////////////////
    '//ファイル名を半角にする
    Private Sub RenameHankaku()
        For Each strfile As String In IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text)
            If IO.Path.GetFileName(strfile) <> _
                StrConv(IO.Path.GetFileName(strfile), VbStrConv.Narrow) Then

                IO.Directory.Move(strfile, Me.lblReceiptFilesPath.Text & "\" & _
                                  StrConv(IO.Path.GetFileName(strfile), VbStrConv.Narrow))
            End If

            '半角スペースを削除
            If IO.Path.GetFileName(strfile).Contains(Space(1)) Then
                Dim fn As String = IO.Path.GetFileName(Replace(strfile, Space(1), String.Empty))
                IO.Directory.Move(strfile, _
                                  Me.lblReceiptFilesPath.Text & "\" & _
                                  fn)

            End If


        Next

    End Sub
#End Region

#Region "ファイル移動"

    ''' <summary>
    ''' ファイルをフォルダに移動
    ''' </summary>
    ''' <param name="strFileName">もとファイル名</param>
    ''' <param name="strFolderName">入れるフォルダ</param>
    ''' <remarks></remarks>
    Private Sub MoveFiles(ByVal strFileName As String, ByVal strFolderName As String)


        Dim xlsx() As String
        Dim xls2() As String
        Dim docx() As String
        Dim doc() As String
        Dim pdf() As String

        'セット番号フォルダ作成時は、赤線だろうが何だろうがフォルダに分ける
        Select Case True
            Case rb_set.Checked
                'xlsx = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "-*.xlsx")
                'xls2 = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "-*.xls")
                'docx = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "*.docx")
                'doc = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "*.doc")
                'pdf = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "*.pdf")

                xlsx = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "-*.xlsx", IO.SearchOption.AllDirectories)
                xls2 = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "-*.xls", IO.SearchOption.AllDirectories)
                docx = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "*.docx", IO.SearchOption.AllDirectories)
                doc = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "*.doc", IO.SearchOption.AllDirectories)
                pdf = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "*.pdf", IO.SearchOption.AllDirectories)
            Case Else
                'xlsx = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "-??.xlsx")
                'xls2 = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "-??.xls")
                'docx = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & ".docx")
                'doc = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & ".doc")
                'pdf = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & ".pdf")

                xlsx = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "-??.xlsx", IO.SearchOption.AllDirectories)
                xls2 = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "-??.xls", IO.SearchOption.AllDirectories)
                docx = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & ".docx", IO.SearchOption.AllDirectories)
                doc = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & ".doc", IO.SearchOption.AllDirectories)
                pdf = IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & ".pdf", IO.SearchOption.AllDirectories)

        End Select


        'Dim xlsx() As String = _
        '    IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "-??.xlsx")

        Dim xlsx2() As String = _
            IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & ".xlsx")

        Dim xls1() As String = _
            IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & ".xls")

        'Dim xls2() As String = _
        '    IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & "-??.xls")

        'Dim docx() As String = _
        '    IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & ".docx")

        'Dim doc() As String = _
        '    IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & ".doc")

        'Dim pdf() As String = _
        '    IO.Directory.GetFiles(Me.lblReceiptFilesPath.Text, strFileName & ".pdf")

        Dim cnt As Integer = 0



        '項番フォルダとセット番号フォルダに分岐
        Dim flgCreate As Boolean = False

        Select Case True
            Case rb_koban.Checked
                '項番フォルダはエクセルがないと作成フラグあげない
                If xlsx.Length > 0 Or xlsx2.Length > 0 Then
                    flgCreate = True
                Else
                    flgCreate = False
                End If

            Case rb_set.Checked
                'セット番号フォルダはPDFがなかったら作成フラグあげない
                If pdf.Length > 0 Then
                    flgCreate = True
                Else
                    flgCreate = False
                End If
        End Select

        Dim ex As New ForExcel_kk.clsExcel_kk


        '作成フラグが上がっている場合のみ作成
        If flgCreate Then
            IO.Directory.CreateDirectory(strFolderName)


            For cnt = 0 To xls1.Length - 1
                If Not IO.File.Exists(strFolderName & "\" & IO.Path.GetFileName(xls1(cnt))) Then
                    IO.File.Move(xls1(cnt), strFolderName & "\" & IO.Path.GetFileName(xls1(cnt)))
                    cntXLS += 1
                End If
            Next

            For cnt = 0 To xls2.Length - 1
                If Not IO.File.Exists(strFolderName & "\" & IO.Path.GetFileName(xls2(cnt))) Then
                    IO.File.Move(xls2(cnt), strFolderName & "\" & IO.Path.GetFileName(xls2(cnt)))
                    cntXLS += 1
                End If
            Next


            For cnt = 0 To xlsx.Length - 1
                If Not IO.File.Exists(strFolderName & "\" & IO.Path.GetFileName(xlsx(cnt))) Then
                    IO.File.Move(xlsx(cnt), strFolderName & "\" & IO.Path.GetFileName(xlsx(cnt)))
                    cntXLS += 1
                End If

            Next

            For cnt = 0 To xlsx2.Length - 1
                If Not IO.File.Exists(strFolderName & "\" & IO.Path.GetFileName(xlsx2(cnt))) Then
                    IO.File.Move(xlsx2(cnt), strFolderName & "\" & IO.Path.GetFileName(xlsx2(cnt)))
                    cntXLS += 1
                End If
            Next

            For cnt = 0 To docx.Length - 1
                If Not IO.File.Exists(strFolderName & "\" & IO.Path.GetFileName(docx(cnt))) Then
                    IO.File.Move(docx(cnt), strFolderName & "\" & IO.Path.GetFileName(docx(cnt)))
                    CreateFileMovelist(docx(cnt), strFolderName)
                    cntDOC += 1
                End If

            Next

            For cnt = 0 To doc.Length - 1
                If Not IO.File.Exists(strFolderName & "\" & IO.Path.GetFileName(doc(cnt))) Then
                    IO.File.Move(doc(cnt), strFolderName & "\" & IO.Path.GetFileName(doc(cnt)))
                    cntDOC += 1
                End If

            Next

            For cnt = 0 To pdf.Length - 1
                If Not IO.File.Exists(strFolderName & "\" & IO.Path.GetFileName(pdf(cnt))) Then
                    IO.File.Move(pdf(cnt), strFolderName & "\" & IO.Path.GetFileName(pdf(cnt)))
                    CreateFileMovelist(pdf(cnt), strFolderName)
                    cntPDF += 1
                End If

            Next

        End If

    End Sub
#End Region

#Region "作業表にもとファイル名と移動先ファイル名を書く"
    ''' <summary>
    ''' 作業表にもとファイル名と移動先ファイル名を書く
    ''' </summary>
    ''' <remarks></remarks>
    Private Function UpdateSagyohyo() As Boolean

        Dim clsex2 As New ForExcel_kk.clsExcel_kk

        Try
            sensitive(False)
            'エクセルにも反映
            clsex2.CreateExcelNoSave()

            If Not clsex2.OpenWorkBook(Me.lblManageFile.Text, "作業表") Then Exit Function
            dtBeforeFiles = dsFile.Tables(0).Clone
            dtBeforeFiles.PrimaryKey = Nothing


            For Each dt As DataTable In dsFile.Tables
                For Each dr As DataRow In dt.Rows
                    If IsDBNull(dr("翻訳直後フォルダ")) Then Continue For

                    dtBeforeFiles.ImportRow(dr)

                Next
            Next
            clsex2.SetCellValue("作業表", dtBeforeFiles)
            Return True

        Catch ex As Exception

            MessageBox.Show("UpdateSagyohyo" & vbCrLf & ex.Message)
            Return False
        Finally
            clsex2 = Nothing
            sensitive(True)
        End Try

    End Function
#End Region

#Region "もとファイル名と移動先ファイル名をdsFileに控えておく"


    ''' <summary>
    ''' もとファイル名と移動先ファイル名を控えておく
    ''' </summary>
    ''' <param name="strFileName">もとファイル名</param>
    ''' <param name="strFolderName">移動先</param>
    ''' <remarks></remarks>
    Private Sub CreateFileMovelist(ByVal strFileName As String, ByVal strFolderName As String)

        For Each dt As DataTable In dsFile.Tables

            'もとファイル名から項番取得
            Dim fn As String = IO.Path.GetFileNameWithoutExtension(strFileName).Replace("-", String.Empty)

            If fn.Length > 11 Then fn = fn.Substring(0, 11)

            '項番でフィルタ
            Dim dr As DataRow() = dt.Select("項番='" & fn & "'")

            '項番があったらDataTableに入れる
            If dr.Length > 0 Then
                '移動先文字列の作成
                Dim strafter As String = IO.Path.GetFileName(strFolderName) & "\" & IO.Path.GetFileName(strFileName)

                dr(0)("set_folder") = IO.Path.GetFileName(strFolderName)

                'セット番号フォルダに入るファイル名を取得しておく
                Select Case IO.Path.GetExtension(strFileName)
                    Case ".pdf", ".PDF"
                        dr(0)("set_pdf") = strafter
                    Case ".docx", ".DOCX", ".doc", ".DOC"
                        dr(0)("set_docx") = strafter
                    Case ".xlsx", ".XLSX", ".xls", ".XLS"
                        dr(0)("set_xlsx") = strafter

                End Select
            End If
        Next

    End Sub
#End Region

#Region "もとファイル名と移動先ファイル名をDBに登録"

    ''' <summary>
    ''' もとファイル名と移動先ファイル名をDBに登録
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function UpdateSettingFolderTable() As Boolean
        Dim cn As New Npgsql.NpgsqlConnection
        Dim tran As Npgsql.NpgsqlTransaction = Nothing
        Dim cmd As New Npgsql.NpgsqlCommand
        Dim sbsql As New System.Text.StringBuilder
        Try
            sensitive(False)

            cn = DBConn()
            If IsNothing(cn) Then Return True


            tran = cn.BeginTransaction()
            cmd.Connection = cn

            For Each dt As DataTable In dsFile.Tables
                For Each r As DataRow In dt.Rows
                    sbsql.Remove(0, sbsql.ToString.Length)
                    If r("翻訳直後フォルダ").ToString = String.Empty Then Continue For

                    sbsql.AppendLine("update settingFolder set ")
                    sbsql.AppendFormat("honyaku_folder='{0}' ", r("翻訳直後フォルダ").ToString)
                    sbsql.AppendFormat(",pdf='{0}' ", r("pdf").ToString)
                    sbsql.AppendFormat(",docx='{0}' ", r("docx").ToString)
                    sbsql.AppendFormat(",xlsx='{0}' ", r("xlsx").ToString)
                    sbsql.AppendFormat(",set_folder='{0}' ", r("set_folder").ToString)
                    sbsql.AppendFormat(",set_pdf='{0}' ", r("set_pdf").ToString)
                    sbsql.AppendFormat(",set_docx='{0}' ", r("set_docx").ToString)
                    sbsql.AppendFormat(",set_xlsx='{0}' ", r("set_xlsx").ToString)
                    sbsql.AppendFormat(",set_date='{0}' ", Now.ToString("yyyyMMdd HHmmss"))
                    sbsql.AppendFormat(" where koban='{0}'", r("項番").ToString.Replace("-", String.Empty))
                    'sbsql.AppendLine(" and set_date is null")
                    cmd.CommandText = sbsql.ToString
                    cmd.ExecuteNonQuery()

                Next
            Next

            tran.Commit()

            Return True

        Catch ex As Exception
            tran.Rollback()
            MessageBox.Show(UpdateSettingFolderTable & vbCrLf & ex.Message)

            Return False

        Finally
            sensitive(True)
            If Not IsNothing(cn) Then cn.Close()
        End Try

    End Function

#End Region

#Region "フォルダ整頓前のパス・ファイル名を取得"
    ''' <summary>
    ''' フォルダ整頓前のパス・ファイル名を取得
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>セット番号フォルダ分け時に走る</remarks>
    Private Function HonyakuFolderSeiton() As Boolean
        Dim strpath As String = String.Empty
        Dim strFolderName() As String
        dsFile.Tables.Clear()

        strpath = lblReceiptFilesPath.Text
        strFolderName = IO.Directory.GetDirectories(strpath)

        If strFolderName.Length <> 0 Then
            '翻訳直後のフォルダ体系を取得
            For Each f As String In strFolderName
                If Not getBeforeFiles(f) Then Return False
            Next
        Else
            'すでに翻訳者の名前フォルダがない場合
            If Not getBeforeFiles(strpath) Then Return False
        End If

        Return True

    End Function
#End Region

#Region "フォルダ整頓前のパス・ファイル名を取得　dsFilesの作成"
    ''' <summary>
    ''' フォルダ整頓前のパス・ファイル名を取得　dsFilesの作成
    ''' </summary>
    ''' <param name="strFolder">翻訳直後のフォルダパス</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getBeforeFiles(ByVal strFolder As String) As Boolean

        Dim dt As New DataTable
        Dim dtpcol(0) As DataColumn
        dtpcol(0) = dt.Columns.Add("項番", System.Type.GetType("System.String"))
        dt.Columns.Add("翻訳直後フォルダ", System.Type.GetType("System.String"))
        dt.Columns.Add("pdf", System.Type.GetType("System.String"))
        dt.Columns.Add("docx", System.Type.GetType("System.String"))
        dt.Columns.Add("xlsx", System.Type.GetType("System.String"))
        dt.Columns.Add("set_Folder", System.Type.GetType("System.String"))
        dt.Columns.Add("set_pdf", System.Type.GetType("System.String"))
        dt.Columns.Add("set_docx", System.Type.GetType("System.String"))
        dt.Columns.Add("set_xlsx", System.Type.GetType("System.String"))
        dt.Columns.Add("koban_pdf", System.Type.GetType("System.String"))
        dt.Columns.Add("koban_docx", System.Type.GetType("System.String"))
        dt.Columns.Add("koban_xlsx", System.Type.GetType("System.String"))

        dt.PrimaryKey = dtpcol
        dt.TableName = IO.Path.GetFileName(strFolder)

        'Dim cn As New Npgsql.NpgsqlConnection
        'cn = DBConn()

        'Dim tran As Npgsql.NpgsqlTransaction = cn.BeginTransaction()
        'Dim cmd As New Npgsql.NpgsqlCommand

        'Dim sbsql As New System.Text.StringBuilder
        Try
            ' cmd.Connection = cn

            'エクセルから読んだ項番を登録
            For r As Integer = 0 To dtExcel.Rows.Count - 1
                Dim dr As DataRow
                dr = dt.NewRow()
                dr("項番") = dtExcel.Rows(r)("項番").ToString
                dt.Rows.Add(dr)
            Next
            dt.AcceptChanges()

            'フォルダよりファイルを取得
            Dim res() As String = _
                IO.Directory.GetFiles(strFolder, "*.*", IO.SearchOption.AllDirectories)

            'dsFileに各ファイル名を入れる
            For r As Integer = 0 To UBound(res)
                Dim dr As DataRow()

                'If Not dt.Rows.Contains(IO.Path.GetFileNameWithoutExtension(res(r))) Then
                Dim fn As String = IO.Path.GetFileNameWithoutExtension(res(r))


                'If fn = "Thumbs" Then Exit For
                If fn = "Thumbs" Then Continue For
                If fn.Length > 11 Then fn = fn.Substring(0, 11)

                dr = dt.Select("項番 = '" & fn & "'")
                If dr.Length = 0 Then Continue For

                'dr = dt.NewRow()
                'dr("項番") = IO.Path.GetFileNameWithoutExtension(res(r))
                dr(0)("翻訳直後フォルダ") = IO.Path.GetFileName(IO.Path.GetDirectoryName(res(r)))
                'dt.Rows.Add(dr)



                Select Case IO.Path.GetExtension(res(r))
                    Case ".pdf", ".PDF"
                        dr(0)("pdf") = IO.Path.GetFileName(res(r))
                    Case ".docx", ".DOCX"
                        dr(0)("docx") = IO.Path.GetFileName(res(r))
                End Select
                'End If
                dt.AcceptChanges()

            Next

            If dsFile.Tables.Contains(strFolder) Then
                dt = dsFile.Tables(strFolder)
            Else
                dsFile.Tables.Add(dt)
            End If


            Return True
        Catch ex As Exception

            MessageBox.Show(ex.Message)
            Return False
        Finally

        End Try

    End Function
#End Region


#Region "DB接続"

    Public Function DBConn() As Npgsql.NpgsqlConnection
        Dim cn As New Npgsql.NpgsqlConnection

        Dim dsSetting As New DataSet
        Dim strConnectionString As String = String.Empty

        Try
            If Not IO.File.Exists("setting.xml") Then
                MessageBox.Show("設定ファイルがありません", Application.ProductName, _
                                MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return Nothing
            Else
                '接続文字列ロード
                'Dim sr As New IO.StreamReader("setting.xml", System.Text.Encoding.GetEncoding("shift-jis"))
                'dsSetting.ReadXml(sr)
                'sr.Close()
                strConnectionString = xmlDs.Tables("connection").Rows(0)("string").ToString()
                'strConnectionString = dsSetting.Tables("connection").Rows(0)("string").ToString

            End If

            cn.ConnectionString = strConnectionString  'CONST_CONNECTION_STRING
            cn.Open()
            lblDB.Text = cn.Host.ToString & "/" & cn.Database.ToString



            Return cn

        Catch exdb As Npgsql.NpgsqlException

            Return Nothing


        Catch ex As Exception
            MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return Nothing

        End Try

    End Function
#End Region


#Region "作業表データをDBに登録・加工"

    Private Function CreateSagyoList() As Boolean

        Dim cn As New Npgsql.NpgsqlConnection
        Dim cmd As New Npgsql.NpgsqlCommand
        Dim sbSQL As New System.Text.StringBuilder
        Dim dtSetNo As New DataTable
        Dim dtColor As New DataTable
        Dim da As New Npgsql.NpgsqlDataAdapter
        Dim tran As Npgsql.NpgsqlTransaction = Nothing
        Try
            cn = DBConn()
            tran = cn.BeginTransaction()

            cmd.Connection = cn
            '//20180518093755 furukawa st ////////////////////////
            '//回数を登録

            '作業表作成
            'If MsgBox("今月初回ですか？ 初回の場合は、作業表データを削除します。", _
            '          MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2) = MsgBoxResult.Yes Then

            '    If MsgBox("作業表データを削除しますか？", _
            '              vbYesNoCancel + MsgBoxStyle.DefaultButton2) = Windows.Forms.DialogResult.Yes Then

            '        sbSQL.Remove(0, sbSQL.ToString.Length)
            '        sbSQL.AppendLine("delete from sagyohyo ")

            '        cmd.CommandText = sbSQL.ToString
            '        cmd.ExecuteNonQuery()
            '    Else
            '        Exit Function
            '    End If
            'End If


            Dim kaisu As String = String.Empty

            If dtExcel.Rows(0)(3).ToString = "001" Then
                kaisu = dtExcel.Rows(0)(2).ToString & "-" & "1"
            Else
                kaisu = dtExcel.Rows(0)(2).ToString & "-" & "2"
            End If
            '//20180518093755 furukawa ed ////////////////////////



            sbSQL.Remove(0, sbSQL.ToString.Length)
            sbSQL.AppendLine("insert into sagyohyo ")
            sbSQL.AppendLine("select ")
            sbSQL.AppendLine("null,")
            sbSQL.AppendLine("項番,")
            sbSQL.AppendLine("null,")
            sbSQL.AppendLine("支部コード,")
            sbSQL.AppendLine("依頼年月,")
            sbSQL.AppendLine("月内通番,")
            sbSQL.AppendLine("委託区分,")
            sbSQL.AppendLine("種別,")
            sbSQL.AppendLine("null,null,null,null,null,null,null,null,null,")
            sbSQL.AppendLine("被保険者記号,")
            sbSQL.AppendLine("被保険者番号,")
            sbSQL.AppendLine("国名,")
            sbSQL.AppendLine("外国語,")
            sbSQL.AppendLine("翻訳,")
            sbSQL.AppendLine("null,null, ")
            sbSQL.AppendLine("被保険者記号 || 被保険者番号, ")

            'ページ番号つける
            'sbSQL.AppendLine("null ")
            sbSQL.AppendLine("null,null,null, ")
            sbSQL.AppendLine("ページ数申請書含 ")

            '//20180518093908 furukawa st ////////////////////////
            '//回数を登録
            sbSQL.AppendFormat(",'{0}' ", kaisu)
            '//20180518093908 furukawa ed ////////////////////////

            sbSQL.AppendLine("from manage ")
            sbSQL.AppendFormat("where 依頼年月='{0}' and (納品日 is null or 納品日='') ", _
                               dtExcel.Rows(0)(2).ToString)
            sbSQL.AppendLine(" and not exists(select * from sagyohyo where sagyohyo.項番=manage.項番) ")
            sbSQL.AppendLine("order by 外国語 desc,月内通番")
            '                           ↑山内要望
            cmd.CommandText = sbSQL.ToString
            cmd.ExecuteNonQuery()



            'settingFolderテーブル作成
            'sbSQL.Remove(0, sbSQL.ToString.Length)
            'sbSQL.AppendFormat("select * from settingFolder where koban='{0}'", dtExcel.Rows(0)("項番").ToString)

            'cmd.CommandText = sbSQL.ToString
            'If cmd.ExecuteScalar() = String.Empty Then


            'sbSQL.Remove(0, sbSQL.ToString.Length)
            'sbSQL.AppendLine("insert into settingFolder (koban) ")
            'sbSQL.AppendLine("select ")
            'sbSQL.AppendLine("項番 ")
            'sbSQL.AppendLine("from manage ")

            'settingFolderテーブルに無いレコードだけを入れる
            sbSQL.Remove(0, sbSQL.ToString.Length)
            sbSQL.AppendLine("insert into settingFolder (koban) ")
            sbSQL.AppendLine("select ")
            sbSQL.AppendLine("項番 ")
            sbSQL.AppendLine("from manage left join settingfolder on manage.項番=settingfolder.koban")
            sbSQL.AppendLine(" where settingfolder.koban is null")



            cmd.CommandText = sbSQL.ToString
            cmd.ExecuteNonQuery()

            ' End If

            'セット番号つける
            sbSQL.Remove(0, sbSQL.ToString.Length)
            sbSQL.AppendLine("select 月内通番,記号番号,委託区分 from sagyohyo ")

            '//20210318133506 furukawa st ////////////////////////
            '//セット番号つける対象を依頼年月で絞らないとループが遅くなる

            sbSQL.AppendFormat(" where 依頼年月='{0}' ", dtExcel.Rows(0)(2).ToString)
            '//20210318133506 furukawa ed ////////////////////////

            sbSQL.AppendLine("group by 委託区分,月内通番,記号番号 order by 委託区分 desc,月内通番")

            cmd.CommandText = sbSQL.ToString
            da.SelectCommand = cmd
            da.Fill(dtSetNo)

            'セット番号最大値取得
            sbSQL.Remove(0, sbSQL.ToString.Length)


            '//20180501141344 furukawa st ////////////////////////
            '//セット番号最大の取得条件変更

            sbSQL.AppendFormat("select MAX(セット番号) from sagyohyo where 依頼年月='{0}' group by 依頼年月", _
                               dtExcel.Rows(0)(2).ToString)
            'sbSQL.AppendLine("select MAX(セット番号) from sagyohyo")
            '//20180501141344 furukawa ed ////////////////////////



            cmd.CommandText = sbSQL.ToString

            Dim setno As Integer = 0
            If IsNothing(cmd.ExecuteScalar()) Then
                setno = 0
            Else
                Integer.TryParse(cmd.ExecuteScalar().ToString, setno)
            End If

            If setno = 0 Then
                setno = 1
            Else
                setno += 1
            End If

            'Dim strcolor As String = "15"
            For r As Integer = 0 To dtSetNo.Rows.Count - 1

                sbSQL.Remove(0, sbSQL.ToString.Length)
                sbSQL.AppendFormat("update sagyohyo set セット番号='{0}' where 記号番号='{1}' and セット番号 is null",
                                   (setno).ToString("000"), dtSetNo.Rows(r)("記号番号").ToString)

                cmd.CommandText = sbSQL.ToString
                Dim cnt As Integer = cmd.ExecuteNonQuery()

                'セット番号は1行超あった場合にインクリメント
                If cnt > 0 Then setno += 1


                '//20180518113523 furukawa st ////////////////////////
                '//色つけは別処理にした

                '    '色番号もつける
                '    If setno Mod 2 = 0 Then
                '        strcolor = "15"
                '    Else
                '        strcolor = "16"
                '    End If

                '    '色は1行のときはNULLとする
                '    If cnt = 1 Then strcolor = "null"

                '    '色番号更新
                '    sbSQL.Remove(0, sbSQL.ToString.Length)
                '    sbSQL.AppendFormat("update sagyohyo set 色={0} where 記号番号='{1}' and 色 is null  and セット番号 is null ", _
                '                       strcolor, dtSetNo.Rows(r)("記号番号").ToString)
                'cmd.CommandText = sbSQL.ToString
                'cmd.ExecuteNonQuery()
                '//20180518113523 furukawa ed ////////////////////////

            Next

            tran.Commit()

            ''作業表
            'CreateSagyoListExcel()
            ''セット委託区分
            'CreateSetItakuExcel()

            MessageBox.Show("作業表作成終了", Application.ProductName, MessageBoxButtons.OK)

            Return True

        Catch ex As Exception
            If Not IsNothing(tran) Then tran.Rollback()
            MessageBox.Show("CreateSagyoList" & vbCrLf & ex.Message & vbCrLf & sbSQL.ToString(), Application.ProductName, MessageBoxButtons.OK)

        Finally
            cn.Close()
            GetCombo()

        End Try



    End Function
#End Region


#Region "色つけ関数 20180518113420 furukawa"

    Private Function SetColor() As Boolean

        Dim cn As New Npgsql.NpgsqlConnection
        Dim cmd As New Npgsql.NpgsqlCommand
        Dim sbSQL As New System.Text.StringBuilder
        Dim dtColor As New DataTable
        Dim da As New Npgsql.NpgsqlDataAdapter
        Dim tran As Npgsql.NpgsqlTransaction = Nothing

        Try
            If cmbKaisu.Text = String.Empty Then
                MessageBox.Show("回数を選択してください", _
                                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Function
            End If

            cn = DBConn()
            tran = cn.BeginTransaction()

            cmd.Connection = cn

            sbSQL.Remove(0, sbSQL.ToString.Length)
            sbSQL.AppendFormat("select セット番号 from sagyohyo where 回数='{0}'" & _
                             " group by セット番号 having count(*)>1 order by セット番号", cmbKaisu.Text)
            cmd.CommandText = sbSQL.ToString
            da.SelectCommand = cmd
            da.Fill(dtColor)

            If dtColor.Rows.Count = 0 Then
                MessageBox.Show("該当の回数のレコードがありません")
                Exit Function
            End If

            Dim strcolor As String = "15"
            Dim flgNext As Integer = 0
            'Dim flgNext As Boolean = False

            For r As Integer = 0 To dtColor.Rows.Count - 1

                '//20210318133106 furukawa st ////////////////////////
                '//色分け処理修正

                Select Case flgNext
                    Case 0
                        strcolor = "36"
                        flgNext = 1
                    Case 1
                        strcolor = "15"
                        flgNext = 3
                    Case 3
                        strcolor = "17"
                        flgNext = 0
                End Select

                'Select Case flgNext
                '    Case True
                '        strcolor = "16"
                '        flgNext = False
                '    Case False
                '        strcolor = "15"
                '        flgNext = True
                'End Select
                '//20210318133106 furukawa ed ////////////////////////

                'セット番号色分け
                sbSQL.Remove(0, sbSQL.ToString.Length)
                sbSQL.AppendFormat("update sagyohyo set 色={0} where セット番号='{1}' and 回数='{2}'", _
                                   strcolor, dtColor.Rows(r)("セット番号").ToString, cmbKaisu.Text)
                cmd.CommandText = sbSQL.ToString
                cmd.ExecuteNonQuery()



                '委託区分またぎ判定
                sbSQL.Remove(0, sbSQL.ToString.Length)
                sbSQL.AppendFormat("select 委託区分 from sagyohyo where セット番号='{0}' and 回数='{1}' group by 委託区分", _
                                    dtColor.Rows(r)("セット番号").ToString, cmbKaisu.Text)
                cmd.CommandText = sbSQL.ToString
                cmd.ExecuteNonQuery()
                Dim dtitaku As New DataTable
                Dim daitaku As New Npgsql.NpgsqlDataAdapter
                daitaku.SelectCommand = cmd
                daitaku.Fill(dtitaku)
                Dim flgYo As Boolean = False
                Dim flgFuyo As Boolean = False

                For rdt As Integer = 0 To dtitaku.Rows.Count - 1
                    If dtitaku.Rows(rdt)("委託区分").ToString = "2" Then flgFuyo = True
                    If dtitaku.Rows(rdt)("委託区分").ToString = "1" Then flgYo = True
                Next

                If flgFuyo And flgYo Then
                    sbSQL.Remove(0, sbSQL.ToString.Length)
                    sbSQL.AppendFormat("update sagyohyo set 色={0} ,memo1='{3}' where セット番号='{1}' and 回数='{2}'", _
                                       "22", dtColor.Rows(r)("セット番号").ToString, cmbKaisu.Text, "翻訳不要/要またぎ")
                    cmd.CommandText = sbSQL.ToString
                    cmd.ExecuteNonQuery()
                End If

                flgFuyo = False
                flgYo = False


                '回数またぎ判定=2回め取込時しか判定できなさそう
                'sbSQL.Remove(0, sbSQL.ToString.Length)
                'sbSQL.AppendFormat("select 記号番号,回数 from sagyohyo where セット番号='{0}'" & _
                '                   " and 依頼年月='{1}' group by 記号番号,回数", _
                '                    dtColor.Rows(r)("セット番号").ToString, cmbKaisu.Text.Substring(0, 4))
                'cmd.CommandText = sbSQL.ToString
                'cmd.ExecuteNonQuery()
                'Dim dtKaisu As New DataTable
                'Dim daKaisu As New Npgsql.NpgsqlDataAdapter
                'daKaisu.SelectCommand = cmd
                'daKaisu.Fill(dtKaisu)
                'Dim flg1 As Boolean = False
                'Dim flg2 As Boolean = False

                'For rdt As Integer = 0 To dtKaisu.Rows.Count - 1
                '    If dtKaisu.Rows(rdt)("回数").ToString.Substring(5, 1) = "2" Then flg2 = True
                '    If dtKaisu.Rows(rdt)("回数").ToString.Substring(5, 1) = "1" Then flg1 = True
                'Next

                'If flg1 And flg2 Then
                '    sbSQL.Remove(0, sbSQL.ToString.Length)
                '    sbSQL.AppendFormat("update sagyohyo set 色={0} ,memo1='{3}' where セット番号='{1}' and 回数='{2}'", _
                '                       "32", dtColor.Rows(r)("セット番号").ToString, cmbKaisu.Text, "回数またぎ")
                '    cmd.CommandText = sbSQL.ToString
                '    cmd.ExecuteNonQuery()
                'End If

                'flg1 = False
                'flg2 = False

         


            Next


            '//20180530114454 furukawa st ////////////////////////
            '//1行しかないセット番号の色をnullにする

            sbSQL.Remove(0, sbSQL.ToString.Length)
            sbSQL.AppendFormat("update sagyohyo set 色=null where 回数='{0}' and セット番号 in " & _
                               "(select セット番号 from sagyohyo where 回数='{0}' group by セット番号 having count(*)=1)",
                               cmbKaisu.Text)
            cmd.CommandText = sbSQL.ToString
            cmd.ExecuteNonQuery()
            '//20180530114454 furukawa ed ////////////////////////

            tran.Commit()

            MessageBox.Show("色付け終了", _
                            Application.ProductName, MessageBoxButtons.OK)

            Return True
        Catch ex As Exception
            If Not IsNothing(tran) Then tran.Rollback()
            MessageBox.Show("SetColor" & vbCrLf & ex.Message & vbCrLf & sbSQL.ToString(), _
                            Application.ProductName, MessageBoxButtons.OK)

        Finally
            cn.Close()

        End Try
    End Function
#End Region


#Region "エクセルに作業表を作る"

    Private Function CreateSagyoListExcel() As Boolean
        Dim dtSagyo As New DataTable
        Dim cn As New Npgsql.NpgsqlConnection
        Dim cmd As New Npgsql.NpgsqlCommand
        Dim sbSQL As New System.Text.StringBuilder
        Dim da As New Npgsql.NpgsqlDataAdapter

        Try

            If cmbKaisu.Text = String.Empty Then
                MsgBox("回数を選択してください")
                Exit Function
            End If
            If cmbItaku.Text = String.Empty Then
                MsgBox("委託区分を選択してください")
                Exit Function
            End If

            sensitive(False)

            cn = DBConn()
            cmd.Connection = cn

            'sbSQL.AppendLine("select * from sagyohyo order by セット番号,月内通番")
            sbSQL.AppendFormat("select * from sagyohyo where 回数='{0}' and 委託区分='{1}' order by セット番号,月内通番", _
                               Me.cmbKaisu.Text, Me.cmbItaku.Text)

            cmd.CommandText = sbSQL.ToString
            da.SelectCommand = cmd
            da.Fill(dtSagyo)

            If Not clsex.CreateSagyoSheet("作業表", dtSagyo, lblManageFile.Text) Then
                MessageBox.Show("作業表作成失敗", Application.ProductName, _
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

            Return True

        Catch ex As Exception
            MessageBox.Show("CreateSagyoListExcel" & vbCrLf & ex.Message, Application.ProductName, _
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        Finally
            cn.Close()
            clsex.CloseExcel()
            sensitive(True)
        End Try


    End Function
#End Region

#Region "エクセルに委託区分表を作る"

    Private Function CreateSetItakuExcel() As Boolean
        Dim ds As New DataSet
        Dim cn As New Npgsql.NpgsqlConnection
        Dim cmd As New Npgsql.NpgsqlCommand
        Dim sbSQL As New System.Text.StringBuilder
        Dim da As New Npgsql.NpgsqlDataAdapter

        Try
            sensitive(False)
            cn = DBConn()
            cmd.Connection = cn

            '//20180501142656 furukawa st ////////////////////////
            '//委託区分取得条件変更
            'sbSQL.AppendLine("select セット番号 from sagyohyo where 委託区分='1' group by セット番号 order by セット番号")
            sbSQL.AppendFormat("select セット番号 from sagyohyo where " & _
                               "回数='{0}' and 委託区分='1' group by セット番号 order by セット番号", _
                               Me.cmbKaisu.Text)
            '//20180501142656 furukawa ed ////////////////////////

            cmd.CommandText = sbSQL.ToString
            da.SelectCommand = cmd
            da.Fill(ds, "委託区分1")

            sbSQL.Remove(0, sbSQL.ToString.Length)
            '//20180501142656 furukawa st ////////////////////////
            '//委託区分取得条件変更

            'sbSQL.AppendLine("select セット番号 from sagyohyo where 委託区分='2' group by セット番号 order by セット番号")
            sbSQL.AppendFormat("select セット番号 from sagyohyo where " & _
                               "回数='{0}' and 委託区分='2' group by セット番号 order by セット番号", _
                               Me.cmbKaisu.Text)
            '//20180501142656 furukawa ed ////////////////////////

            cmd.CommandText = sbSQL.ToString
            da.SelectCommand = cmd
            da.Fill(ds, "委託区分2")


            sbSQL.Remove(0, sbSQL.ToString.Length)

            '//20180501142656 furukawa st ////////////////////////
            '//委託区分取得条件変更

            'sbSQL.AppendLine("select A.セット番号 from (select セット番号 from sagyohyo where 委託区分='1') A ")
            'sbSQL.AppendLine("inner join (select セット番号 from sagyohyo where 委託区分='2') B ")
            'sbSQL.AppendLine("on A.セット番号=B.セット番号 group by A.セット番号,B.セット番号  order by A.セット番号")

            sbSQL.AppendFormat("select A.セット番号 from (select セット番号 from sagyohyo where 回数='{0}' and 委託区分='1') A ", Me.cmbKaisu.Text)
            sbSQL.AppendFormat("inner join (select セット番号 from sagyohyo where 回数='{0}'  and 委託区分='2') B ", Me.cmbKaisu.Text)
            sbSQL.AppendLine("on A.セット番号=B.セット番号 group by A.セット番号,B.セット番号  order by A.セット番号")

            '//20180501142656 furukawa ed ////////////////////////

            cmd.CommandText = sbSQL.ToString
            da.SelectCommand = cmd
            da.Fill(ds, "委託区分12")

            If Not clsex.CreateSetItakuSheet("セット番号フォルダ委託区分", ds, lblManageFile.Text) Then
                MessageBox.Show("委託区分シート作成失敗", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If


            Return True
        Catch ex As Exception
            MessageBox.Show("委託区分シート作成" & vbCrLf & ex.Message)
            Return False
        Finally
            cn.Close()
            clsex.CloseExcel()
            sensitive(True)
        End Try


    End Function
#End Region

#Region "XML読書"
    Private Function LoadXml() As Boolean
        Try
            xmlDs.ReadXml("setting.xml")

            lblReceiptFilesPath.Text = xmlDs.Tables("path").Rows(0)("rezeptFolder").ToString
            lblFolderCreatePath.Text = xmlDs.Tables("path").Rows(0)("CreateFolder").ToString
            lblManageFile.Text = xmlDs.Tables("path").Rows(0)("manageFile").ToString

            strKimitusei = xmlDs.Tables("additional").Rows(0)("kimitusei").ToString
            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, Application.ProductName, _
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

    End Function
    Private Function Write2Xml(strTable As String, strFld As String, strVal As String) As Boolean
        Try


            If strVal = String.Empty Then Exit Function
            xmlDs.Tables(strTable).Rows(0)(strFld) = strVal
            xmlDs.WriteXml("setting.xml")
            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, Application.ProductName, _
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Function
#End Region

#Region "一覧表ボタン(未使用)"

    Private Sub btnItiran_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnItiran.Click
        Dim frm2 As New frmList
        frm2.Show()
    End Sub
#End Region

#Region "合計点数取得"

    'Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

    Private Function GetScore() As Boolean

        Dim KobanFolder() As String

        Dim intScore As Integer = 0
        Dim dt As New DataTable
        Dim clsex As New ForExcel_kk.clsExcel_kk
        Dim exa As Object = Nothing

        Try

            exa = clsex.CreateExcelNoSave()

            KobanFolder = IO.Directory.GetDirectories(lblReceiptFilesPath.Text)
            dt.TableName = "sumScore"
            dt.Columns.Add("項番")
            dt.Columns.Add("Chk合計点数")

            For Each fld As String In KobanFolder
                Dim dr As DataRow = dt.NewRow
                dr("項番") = IO.Path.GetFileName(fld).Replace("-", String.Empty)

                For Each s As String In IO.Directory.GetFiles(fld, "*.xls", IO.SearchOption.AllDirectories)

                    clsex.OpenWorkBook(s, "【別紙４】結果回答書")
                    intScore += clsex.GetCellValue("【別紙４】結果回答書", "f49")

                Next
                dr("Chk合計点数") = intScore
                intScore = 0
                dt.Rows.Add(dr)
            Next
            dt.AcceptChanges()


            If Not clsex.OpenWorkBook(Me.lblManageFile.Text, strSelectSheet) Then Exit Function
            'If Not clsex.OpenWorkBook(Me.lblManageFile.Text, "作業表") Then Exit Function
            'If Not clsex.SetCellValue("作業表", dt) Then Exit Function

            Dim lstErr As List(Of String) = clsex.SetCellValue(strSelectSheet, dt)
            'Dim lstErr As List(Of String) = clsex.SetCellValue("作業表", dt)

            'If lstErr.Count > 0 Then
            '    Dim dlg As New dlgErr
            '    dlg.txt.Text &= "合計点数取得" & vbCrLf

            '    For Each s As String In lstErr
            '        dlg.txt.Text &= s & vbCrLf
            '    Next
            '    dlg.Show()
            'End If

            Return True

        Catch ex As Exception
            MessageBox.Show("GetScore" & vbCrLf & ex.Message)
            Return False
        Finally
            clsex.CloseExcelNoSave(exa)
        End Try

    End Function
#End Region


#Region "バージョン取得"


    Private Sub GetVer()
        If Not IO.File.Exists(Application.StartupPath & "\txtVer.txt") Then
            tssl.Text = "バージョンファイルがありません"
            Exit Sub
        End If

        Dim sr As IO.StreamReader
        Dim strVer As String = String.Empty

        sr = New IO.StreamReader("txtVer.txt", System.Text.Encoding.GetEncoding("shift-jis"))
        Do While Not sr.EndOfStream
            strVer = sr.ReadLine
        Loop

        sr.Close()
        tssl.Text = "Ver." & strVer.Trim


    End Sub

#End Region

#End Region

End Class
