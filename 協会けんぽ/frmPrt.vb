﻿Public Class frmPrt
    Public dtKoban As New DataTable

    Dim strFolder As String
    Dim dtSrc As New DataTable
    Dim lstFolderName As New List(Of String)
    Dim lstFolderPath As New List(Of String)
    Dim strPrinter As String '既定のプリンタ名称
    Dim strPrintEx As String = String.Empty '印字したファイル種類
    Dim strPrintHourNameEx As String = String.Empty 'db書き込み用


#Region "コンストラクタ"

    Public Sub New(ByVal _dt As DataTable, ByVal _strFolderPath As String)

        ' この呼び出しは、Windows フォーム デザイナで必要です。
        InitializeComponent()

        ' InitializeComponent() 呼び出しの後で初期化を追加します。
        dtKoban = _dt
        strFolder = _strFolderPath

    End Sub
#End Region

#Region "フォームロード"

    Private Sub frmPrt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load



        If Not InitDGV() Then Exit Sub

        If Not InitPrintRange() Then
            Me.Close()
            Exit Sub
        End If


        ChkNone()


    End Sub
#End Region

#Region "センシティブ"
    Private Sub Sensitive(flg As Boolean)
        For Each ctl As Control In Me.Controls
            ctl.Enabled = flg
        Next
    End Sub
#End Region


#Region "印刷範囲"


    Private Function InitPrintRange() As Boolean

        Dim maxTuban As Integer = 0
        Dim minTuban As Integer = 1

        If dgv.Rows.Count <= 0 Then
            MessageBox.Show("項番に合致するファイルが存在しません", Application.ProductName, _
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If


        For r As Integer = 0 To dgv.Rows.Count - 1
            Dim tmp As Integer
            If IsNumeric(dgv.Rows(r).Cells("月内通番").Value) Then
                tmp = dgv.Rows(r).Cells("月内通番").Value

                If maxTuban <= tmp Then maxTuban = tmp

                If minTuban >= tmp Then minTuban = tmp

            End If

        Next

        Me.numPrt1.Maximum = maxTuban
        Me.numPrt1.Value = minTuban
        Me.numPrt2.Maximum = maxTuban
        Me.numPrt2.Value = maxTuban
        Return True

    End Function
#End Region

#Region "対応項番がないフォルダをチェック"
    Private Sub ChkNone()
        For r As Integer = 0 To dgv.Rows.Count - 1
            If dgv.Rows(r).Cells("管理表の項番").Value.Equals(DBNull.Value) Then
                dgv.Rows(r).DefaultCellStyle.BackColor = Color.Salmon
                Continue For
            End If

            If dgv.Rows(r).Cells("管理表の項番").Value = String.Empty Then
                dgv.Rows(r).DefaultCellStyle.BackColor = Color.Salmon
            End If
        Next
    End Sub
#End Region

#Region "一覧初期化"
    Private Function InitDGV() As Boolean
        Try


            'フォルダ名の取得
            lstFolderPath.AddRange(IO.Directory.GetDirectories(strFolder))
            lstFolderName.AddRange(IO.Directory.GetDirectories(strFolder))

            For r As Integer = 0 To lstFolderName.Count - 1
                lstFolderName(r) = IO.Path.GetFileName(lstFolderName(r))
            Next

            dtSrc.Columns.Add("印刷", System.Type.GetType("System.Boolean"))
            dtSrc.Columns.Add("月内通番", System.Type.GetType("System.String"))
            dtSrc.Columns.Add("項番フォルダ名", System.Type.GetType("System.String"))
            dtSrc.Columns.Add("管理表の項番", System.Type.GetType("System.String"))
            dtSrc.Columns.Add("印刷処理日時", System.Type.GetType("System.String"))
            dtSrc.Columns.Add("印刷したファイル", System.Type.GetType("System.String"))
            dtSrc.Columns.Add("フォルダパス", System.Type.GetType("System.String"))


            '項番フォルダ名と、管理表に記載されている項番を取得
            For r As Integer = 0 To dtKoban.Rows.Count - 1

                If r >= lstFolderName.Count Then Exit For

                Dim dr As DataRow
                dr = dtSrc.NewRow

                dr("項番フォルダ名") = lstFolderName(r)
                dr("フォルダパス") = lstFolderPath(r)

                '合致した項番のみ項番名を出す
                Dim strFolderKoban As String = String.Empty
                If lstFolderName(r).Length < 10 Then Continue For

                strFolderKoban = StrConv(lstFolderName(r).Replace("-", String.Empty).Substring(0, 11), VbStrConv.Narrow)

                Dim tmpdr() As DataRow = dtKoban.Select("項番='" & strFolderKoban & "'")
                If tmpdr.Length > 0 Then
                    dr("管理表の項番") = tmpdr(0)("項番")
                    If dtKoban.Columns.Contains("月内" & vbLf & "通番") Then
                        dr("月内通番") = tmpdr(0)("月内" & vbLf & "通番")
                    ElseIf dtKoban.Columns.Contains("月内通番") Then
                        dr("月内通番") = tmpdr(0)("月内通番")
                    Else
                        MsgBox("月内通番がありません", MsgBoxStyle.Exclamation)
                        Return False
                    End If

                End If

                '一旦全行初期化
                dr("印刷") = False

                dtSrc.Rows.Add(dr)

            Next


            Me.dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray

            Me.dgv.DataSource = dtSrc
            Me.dgv.Columns("印刷").Width = 40
            Me.dgv.Columns("月内通番").Width = 80
            Me.dgv.Columns("項番フォルダ名").Width = 130
            Me.dgv.Columns("管理表の項番").Width = 130
            Me.dgv.Columns("印刷処理日時").Width = 200
            Me.dgv.Columns("印刷したファイル").Width = 130
            Me.dgv.Columns("フォルダパス").Width = 300
            Return True

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False

        End Try
    End Function
#End Region



#Region "全選択チェックボックス"


    Private Sub chkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged

        For r As Integer = 0 To dgv.Rows.Count - 1
            dgv.Rows(r).Cells("印刷").Value = chkAll.Checked
        Next

    End Sub
#End Region

#Region "印刷ボタン"

    Private Sub btnPrt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrt.Click


        If pd.ShowDialog() = Windows.Forms.DialogResult.OK Then


            strPrinter = pd.PrinterSettings.PrinterName

            Dim prd As New System.Drawing.Printing.PrintDocument
            prd.DefaultPageSettings.Color = False
            prd.DefaultPageSettings.PrinterSettings.PrinterName = strPrinter


            SetDefaultPrinter(strPrinter)

            If Not PrintFolder() Then Exit Sub
        Else
            Exit Sub
        End If

        '印字結果
        OutputDataSource()

        MsgBox("印刷終了", MsgBoxStyle.Information)

    End Sub
#End Region

#Region "印字結果ファイル"
    Private Sub OutputDataSource()
        Dim sw As New IO.StreamWriter(Now.ToString("yyyyMMdd-HHmmss") & "-printout.csv", _
                                      False, System.Text.Encoding.GetEncoding("shift-jis"))

        For Each dr As DataRow In dtSrc.Rows

            Dim strline As String = String.Empty

            For c As Integer = 0 To dr.Table.Columns.Count - 1
                strline &= dr(c).ToString() & ","
            Next
            strline = strline.Substring(0, strline.Length - 1)
            sw.WriteLine(strline)
        Next
        sw.Close()

    End Sub
#End Region


#Region "既定のプリンタ設定"
    'https://dobon.net/vb/dotnet/graphics/defaultprinter.html#section5

    Public Sub SetDefaultPrinter(ByVal printerName As String)
        'WshNetworkオブジェクトを作成する
        Dim t As Type = Type.GetTypeFromProgID("WScript.Network")
        Dim wshNetwork As Object = Activator.CreateInstance(t)
        'SetDefaultPrinterメソッドを呼び出す
        t.InvokeMember("SetDefaultPrinter", _
            System.Reflection.BindingFlags.InvokeMethod, _
            Nothing, wshNetwork, New Object() {printerName})
    End Sub
#End Region

#Region "項番フォルダ単位で印刷"
    Private Function PrintFolder() As Boolean


        Try

            Sensitive(False)

            For r As Integer = 0 To lstFolderPath.Count - 1
                Dim f As New List(Of String)
                If dgv.Rows.Count <= r Then Exit For
                If dgv.Rows(r).Cells("印刷").Value = True Then
                    f.AddRange(IO.Directory.GetFiles(dgv.Rows(r).Cells("フォルダパス").Value.ToString))

                    Dim f_sort = f.OrderBy(Function(s) s).ThenByDescending(Function(s) AscW(s(s.Length - 3)))

                    'フォルダ内のファイルをリストに入れて印刷処理
                    Printing(f)

                    '印刷結果文字列
                    strPrintHourNameEx = Now.ToString("yy/MM/dd HH:mm:ss") & Space(1) & strPrinter
                    dgv.Rows(r).Cells("印刷処理日時").Value = strPrintHourNameEx
                    dgv.Rows(r).Cells("印刷したファイル").Value = strPrintEx
                    strPrintHourNameEx &= Space(1) & strPrintEx

                    If Not UpdateDB(strPrintHourNameEx, _
                                    dgv.Rows(r).Cells("管理表の項番").Value.ToString) Then Exit Function

                End If

            Next

            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, Application.ProductName, _
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False

        Finally
            Sensitive(True)
        End Try

    End Function
#End Region

#Region "印刷処理(VBScriptコール)"
    Private Sub Printing(ByVal lst As List(Of String))

        strPrintEx = String.Empty
        Dim cntExcel As Integer = 0, cntWord As Integer = 0, cntPDF As Integer = 0
 

        For Each strFile As String In lst

            'excel,word,pdfの各カウントを取り、どのファイルを印字したか取得
            Select Case IO.Path.GetExtension(strFile)
                Case ".xlsx", ".xls"

                    If Not chkExcel.Checked Then
                        Continue For
                    Else
                        cntExcel += 1
                        lstProgAdd(System.IO.Path.GetFileName(strFile))
                    End If

                Case ".docx", ".doc"

                    If Not chkWord.Checked Then
                        Continue For
                    Else
                        cntWord += 1
                        lstProgAdd(System.IO.Path.GetFileName(strFile))
                    End If

                Case ".pdf"

                    If Not chkPDF.Checked Then
                        Continue For
                    Else
                        cntPDF += 1
                        lstProgAdd(System.IO.Path.GetFileName(strFile))
                    End If

            End Select


            'VBSを呼び出し印字
            Dim psi As New ProcessStartInfo("WScript.exe")
            psi.Arguments = "printfile.vbs """ & strFile & """"

            Dim job As Process = Process.Start(psi)
            job.WaitForExit()
        Next

        If cntExcel > 0 Then strPrintEx &= "Excel "
        If cntWord > 0 Then strPrintEx &= "Word "
        If cntPDF > 0 Then strPrintEx &= "PDF "

    End Sub
#End Region

#Region "進捗状況修正 20180717132721 furukawa"

    Private Sub lstProgAdd(strFile As String)
        lstProg.Items.Add(System.IO.Path.GetFileName(strFile))
        lstProg.SelectedIndex = lstProg.Items.Count - 1
    End Sub
#End Region


#Region "印刷範囲ボタン"


    Private Sub btnChk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChk.Click

        Dim bs As New BindingSource
        bs.DataSource = dgv.DataSource

        For tuban As Integer = numPrt1.Value To numPrt2.Value

            Dim idx As Integer = bs.Find("月内通番", tuban.ToString("000"))
            If idx >= 0 Then dgv.Rows(idx).Cells("印刷").Value = True

        Next
    End Sub
#End Region

#Region "印字したことを管理表テーブルに更新"

    Private Function UpdateDB(ByVal strUpd As String, _
                              ByVal strKoban As String) As Boolean
        Dim cn As New Npgsql.NpgsqlConnection
        Dim tran As Npgsql.NpgsqlTransaction = Nothing
        Dim cmd As New Npgsql.NpgsqlCommand
        Dim sbsql As New System.Text.StringBuilder
        Try

            cn = frmOpe.DBConn()
            If IsNothing(cn) Then Return True


            cmd.Connection = cn
            sbsql = sbsql.Remove(0, sbsql.ToString.Length)

            sbsql.AppendFormat("update manage set 印刷='{0}' where 項番='{1}'", _
                               strUpd, strKoban)

            cmd.CommandText = sbsql.ToString
            cmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            If Not IsNothing(cn) Then cn.Close()

        End Try

    End Function
#End Region



#Region "進捗クリアボタン"


    Private Sub btnProgClear_Click(sender As Object, e As EventArgs) Handles btnProgClear.Click
        If MessageBox.Show("進捗をクリアしますか？", Application.ProductName, _
                           MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
            lstProg.Items.Clear()
        End If


    End Sub
#End Region

End Class