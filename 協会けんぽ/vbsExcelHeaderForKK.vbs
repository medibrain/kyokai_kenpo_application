option explicit
on error resume next

dim objExcel,objWB,objWS
dim args
dim fso
dim strFullFilePath

dim strKimitusei'【機密性３】はVBから取得する
Const wdSeekCurrentPageHeader=9
Const Common_Protect_Password1 = "2021idem"
Const Common_Protect_Password2 = "medi1202"


dim txtfile

const xlLineStyleNone=-4142 '線なし
const xlContinuous=1'実線
const xlEdgeBottom=9'下線
const xlEdgeTop=8'上線
 

dim rngKomoku

set args=Wscript.Arguments
set fso=CreateObject("scripting.FileSystemObject")

strKimitusei=split(args(0),",")(1)
strFullFilePath=split(args(0),",")(0)

dim strExtension
strExtension=fso.GetExtensionName(strFullFilePath)'ファイルの拡張子を取得

'excelでない場合抜ける
if strExtension <>"xlsx" then 
    Wscript.Quit
    wscript.sleep 1000
end if

'一時ファイルの場合抜ける
'if instr(args,"~$")>0 then
 '   Wscript.Quit
  '  wscript.sleep 100
'end if


'純粋なファイル名だけを抽出
dim fn
fn=replace(fso.GetFileName(strFullFilePath),"." & strExtension,"")


set objExcel=CreateObject("excel.application")
objExcel.visible=false
set objWB=objExcel.Workbooks.Open(strFullFilePath)




'全シートのヘッダを設定
for each objWS in objWB.sheets
	'//20181019172041 furukawa st ////////////////////////
    '//先頭ページのみ別指定の解除
    objWS.PageSetup.DifferentFirstPageHeaderFooter =false
    '//20181019172041 furukawa ed ////////////////////////
	
    objWS.pagesetup.leftheader=strKimitusei
    objWS.pagesetup.rightheader=fn  

'    if objWS.name="別紙2" then SetFormat(objWB.sheets("別紙2"))
'    if objWS.name="【別紙４】結果回答書" then SetFormatBessi4(objWB.sheets("【別紙４】結果回答書"))

'next

'for each objWS in objWB.sheets
    if objWS.name="別紙2" then SetFormat(objWB.sheets("別紙2"))
    if objWS.name="【別紙４】結果回答書" then SetFormatBessi4(objWB.sheets("【別紙４】結果回答書"))
next

'set objWS=objWB.sheets("【別紙４】結果回答書")
'objWS.pagesetup.leftheader=strKimitusei
'objWS.pagesetup.rightheader=fn

'//20180710155251 furukawa st ////////////////////////
'//別紙2のスクロールを初期化しておく
if objWB.worksheets.count>1 then
    objWB.sheets(2).activate
    objExcel.activewindow.scrollrow=1
    objExcel.activewindow.scrollcolumn=1
end if
'//20180710155251 furukawa ed ////////////////////////

objWB.close true
set objWB=nothing

wscript.sleep 1000

objExcel.Quit
set objExcel=nothing

'エラーになったファイル名を返す
dim errfn

if err.number<>0 then
    errfn=fn
    if objWB is not nothing then
        objWB.close false
        set objWB=nothing
    end if

    wscript.sleep 100

    objExcel.Quit
    set objExcel=nothing

end if


'//20180718095332 furukawa st ////////////////////////
'//残っているexcel.exeを強制削除
call ProcessKill
'//20180718095332 furukawa ed ////////////////////////

wscript.quit()


'//20180710155502 furukawa 
'別紙4列幅調整
Function SetFormatBessi4(byref ws)
    with ws
        .Columns(1).columnwidth=1
        .Columns(2).columnwidth=1
        .Columns(3).columnwidth=4
        .Columns(4).columnwidth=4
        .Columns(5).columnwidth=4
        
        .Columns(6).columnwidth=3
        .Columns(7).columnwidth=3
        .Columns(8).columnwidth=3

        .Columns(9).columnwidth=4
        .Columns(10).columnwidth=4
        .Columns(11).columnwidth=4
        .Columns(12).columnwidth=4
        .Columns(13).columnwidth=4
        .Columns(14).columnwidth=4
        .Columns(15).columnwidth=4
        .Columns(16).columnwidth=4
    
        .Columns(17).columnwidth=3
        .Columns(18).columnwidth=3
        .Columns(19).columnwidth=3
		.Columns(20).columnwidth=3
		.Columns(21).columnwidth=3
		.Columns(22).columnwidth=3
		.Columns(23).columnwidth=3
		.Columns(23).columnwidth=3
		.Columns(25).columnwidth=3
		.Columns(26).columnwidth=3

  
        '別紙4の特記事項
        .range("C54")=trim(replace(.range("C54"),vbcrlf,""))
        .range("C54").WrapText=true
        .range("C54").ShrinkToFit=true


        '別紙4の医薬品
        .range("F44")=trim(replace(.range("F44"),vbcrlf,""))

        '//20180802101411 furukawa st ////////////////////////
        '//MSゴシックにしたら枠からはみ出るので400バイト以上は9ポイント、700バイト以上は6ポイントにサイズ調整
        if lenb(.range("F44"))>700 then
            .range("F44").Font.size=6
        elseif lenb(.range("F44"))>400 then
            .range("F44").Font.size=9
        end if
        '//20180802101411 furukawa ed ////////////////////////

        'やったら下の罫線が消えるのでやめた
        '.range("F44").WrapText=true
        '.range("F44").ShrinkToFit=true

        '別紙4の診療行為
        .range("F34")=trim(.range("F34"))
        .range("F34").WrapText=true
        .range("F34").ShrinkToFit=true

       

    end with
end function




'//20180710155535 furukawa 
'//別紙2のフォーマット整頓
Function SetFormat(byref ws)

    with ws

        '保護解除（パスワードを複数想定）
        on error resume next
        .Unprotect Common_Protect_Password1
        if err.number<>0 then 
            .Unprotect Common_Protect_Password2
			err.number=0
        end if
		'2018-07-17
        'if err.number<>0 then msgbox err.Description
        

        '縮小して全体を表示
        if not Shrink(ws) then 
            msgbox "縮小して全体を表示 でエラー"
            exit function
        end if 

        '列幅
        if not SetColWidth(ws) then
            msgbox "列幅 でエラー"
            exit function
        end if
	 
        '罫線設定
        LineRange rngKomoku,ws
                
        '保護
        .Protect Common_Protect_Password1
    end with
   
end function


'//20180710155730 furukawa 
'縮小して全体を表示
Function Shrink(byref ws)

    with ws
    
        '医科歯科判別
        dim IkaSika
        select case right(.Range("J2"),1)
            case "C": IkaSika="Sika"
            case else :IkaSika="Ika"
        end select

        '見出しの整形 縮小して全体を表示
	    Set rngKomoku=.Range("$b$15:$g$15,$b$63:$g$63,$b$122:$g$122,$b$181:$g$181,$b$240:$g$240")
	    rngKomoku.ShrinkToFit = True
	    rngKomoku.WrapText = False


        '入力範囲の整形 縮小して全体を表示
        select case IkaSika
            case "C":
                '歯科
        	    Set rngKomoku=.Range("$c$16:$c$53,c$64:c$111,$c$123:$c$170,$c$182:$c$229,$c$241:$c$288")
            case else:
                '//20180914135743 furukawa st ////////////////////////
                '//備考ページ作成に伴う入力欄の拡大対応
                Set rngKomoku=.Range("$c$16:$c$59,c$64:c$119,$c$123:$c$178,$c$182:$c$237,$c$241:$c$296")
                'Set rngKomoku=.Range("$c$16:$c$53,c$64:c$119,$c$123:$c$178,$c$182:$c$237,$c$241:$c$296")
                '//20180914135743 furukawa ed ////////////////////////
        end select

	    rngKomoku.ShrinkToFit = True
	    rngKomoku.WrapText = False

        '個人情報の範囲 縮小して全体を表示
        dim rngKojin
        Set rngKojin = .Range("$c$6:$c$10,$f$7,$e$10,$c$12,$G$13")
        rngKojin.ShrinkToFit = True
	    rngKojin.WrapText = False
        

        '備考欄 折り返して全体を表示
        dim rngBiko        
        
        '//20180914140600 furukawa st ////////////////////////
        '//医科は備考欄が備考ページに移ったので歯科のみにする
        select case IkaSika
            case "C":
                '歯科
                Set rngBiko = .Range("$b$55")
                rngBiko.ShrinkToFit = false
                rngBiko.WrapText = true
        end select
        '//20180914140600 furukawa ed ////////////////////////
    end with


'pInfo ws

    if err.number=0 then
        Shrink=true
    else
        shrink=false
    end if

end function



'//20180710155813 furukawa
'//別紙2の列幅調整
Function SetColWidth(byref ws)

    with ws

	    .Cells.Font.Name = "ＭＳ Ｐゴシック"
	    .Rows(3).RowHeight = 18
	    '列幅指定
	    .Columns(2).ColumnWidth = 16
	    .Columns(3).ColumnWidth = 47
	    .Columns(4).ColumnWidth = 7
	    .Columns(5).ColumnWidth = 5
	    .Columns(6).ColumnWidth = 8.25
	    .Columns(7).ColumnWidth = 5

    end with

    if err.number=0 then
        SetColWidth=true
    else
        SetColWidth=false
    end if


end function


Function pInfo(byref ws)
    dim strName
    dim strBirthday
    dim strHos
    dim strSick
    dim strKoban

    with ws
        strKoban=.range("J2").value
        strName=.Range("C9").value
        strBirthday=.range("c10").value
        strHos=.range("e10").value
        strSick=.range("C12").value
    end with


    dim strWrite
    strWrite="'" & strKoban & "','" & strName & "','" & strHos & "','" & strSick & "','" & strBirthDay & "'"

    CreatepInfoCSV strWrite

end function


Function CreatepInfoCSV(byval strWrite)

    dim path
    path=fso.GetParentFolderName(wscript.scriptfullname)
    'ファイル名
    Dim strFileName
    strFileName = path & "\pInfo.csv"
     

    '出力ストリームの生成・設定（テキスト、UTF-8）
    Dim outStream
    Set outStream =CreateObject("ADODB.Stream")
    outStream.type = 2
    outStream.charset = "UTF-8" '出力ファイルの文字コード設定
    outStream.open

    '入力ストリームの生成・設定（テキスト、UTF-8）
    dim rec
    if fso.FileExists(strFileName) then
        Dim inStream
        Set inStream = CreateObject("ADODB.Stream")

        inStream.type = 2 '1:バイナリデータ 2:テキストデータ
        inStream.charset = "UTF-8" '入力ファイルの文字コード設定

        inStream.open
        inStream.LoadFromFile strFileName '入力ファイルを読み込む
        
        rec=inStream.ReadText

        inStream.Close
        set inStream=nothing
    end if

    if rec="" then 
        rec= strWrite
    else
        rec=rec & strWrite
    end if

    outStream.writeText rec ,1
    outStream.savetoFile strFileName,2

    outStream.Close    
    set outStream=nothing
    
end function




'//20180710155859 furukawa 
'//点数と数量がある行に下罫線
Function LineRange(ByRef rngLine , byref ws )
    Dim c 
    Dim rngBorder 
    
    With ws
    
        For Each c In rngLine.Cells

            '//20180713144145 furukawa st ////////////////////////
            '//罫線を引く範囲
            '//項目〜ほみそ→点数〜ほみそに変更

            '項目〜ほみそを範囲とする
            'Set rngBorder = ws.Range(.Cells(c.Row, c.Column), .Cells(c.Row, c.Column + 4))
            
            '点数〜ほみそを範囲とする
            Set rngBorder = ws.Range(.Cells(c.Row, c.Column+1), .Cells(c.Row, c.Column + 4))
            '//20180713144145 furukawa ed ////////////////////////
            

            '項目〜ほみそになにか入っている場合のみ処理
            if c.offset(0,0).value<>"" or _
                c.offset(0,1).value<>"" or _
                c.offset(0,2).value<>"" or _
                c.offset(0,3).value<>"" or _
                c.offset(0,4).value<>"" then


                '点数がない場合、罫線を引かない
                If c.Offset(0, 1).Value = "" Or c.Offset(0, 2).Value = "" Then
                    rngBorder.Borders(xlEdgeBottom).LineStyle = xlLineStyleNone '線なし
                end if

                '点数と数量があれば下の罫線を引く
                if c.Offset(0, 1).Value <> "" and c.Offset(0, 2).Value <> "" Then
                    rngBorder.Borders(xlEdgeBottom).LineStyle = xlContinuous'実線
                    
                    '//20180727134014 furukawa st ////////////////////////
                    '//点数数量とも0の場合は削除（コメント罫線を引くために入れる）
                    if c.Offset(0, 1).Value = "0" and c.Offset(0, 2).Value = "0" Then                   
                        c.Offset(0, 1).Value ="" 
                        c.Offset(0, 2).Value =""
                    end if
                    '//20180727134014 furukawa ed ////////////////////////
                end if   

                '上のセルと今のセルに文字がない場合は今のセルの上罫線を引く
                If trim(c.Offset(-1, 0).Value) = ""  and trim(c.Offset(0, 0).Value) <> ""  Then
                    rngBorder.Borders(xlEdgeTop).LineStyle = xlContinuous
                end if


'				'2018-07-17 ->前川さんと協議の結果削除
'				'ほみそで上のセルに値がある場合は上の線を引く
'				if trim(c.offset(-1,4).value)<>"" then
'					c.offset(-1,4).borders(xlEdgeTop).LineStyle=xlContinuous
'				end if


                '下が合計の場合は無条件で引く
                If c.Offset(1, 1).Value = "合計（点）" Then
                    rngBorder.Borders(xlEdgeBottom).LineStyle = xlContinuous
                end if

            end if
          
        Next
    End With
End Function



'//20180718095332 furukawa st ////////////////////////
'//残っているexcel.exeを強制削除
sub ProcessKill
    On Error Resume Next
 
    Dim strProcName ' 終了するプロセス名
    Dim objProcList ' プロセス一覧
    Dim objProcess ' プロセス情報
    Dim lngKillNum ' 終了したプロセス数
 
    strProcName = "excel.exe"
    lngKillNum = 0
 
    Set objProcList = GetObject("winmgmts:").InstancesOf("win32_process")
    For Each objProcess In objProcList
        If LCase(objProcess.Name) = strProcName Then
            objProcess.Terminate
            If Err.Number = 0 Then
                lngKillNum = lngKillNum + 1
            Else
                'WScript.Echo "エラー: " &amp; Err.Description
            End If
        End If
    Next
'    If lngKillNum &gt; 0 Then
'        WScript.Echo strProcName &amp; " を " &amp; lngKillNum &amp; " 個強制終了しました。"
'    Else
'        WScript.Echo strProcName &amp; " が見つかりませんでした。"
'    End If
 
    Set objProcList = Nothing
end sub

'//20180718095332 furukawa ed ////////////////////////

'プリンタ設定　結局未使用
sub printer_setting
msgbox  "printer_setting"
    Dim MyPrt 

    MyPrt = objExcel.ActivePrinter
    'objExcel.ActivePrinter = "医科RICOH SP C840M JPN_kk"
    
    'ActiveSheet.PrintOut printtofile:=True, PrToFileName:=ThisWorkbook.Path & "\smple.xps"
     ActiveSheet.PrintOut activesheet.range("a:g"),,,,,"PDF24 PDF",true
   ' objExcel.ActivePrinter = MyPrt
    

end sub

