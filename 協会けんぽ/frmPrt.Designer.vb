﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrt
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
    'Windows フォーム デザイナを使用して変更できます。  
    'コード エディタを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrt))
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.btnPrt = New System.Windows.Forms.Button()
        Me.pd = New System.Windows.Forms.PrintDialog()
        Me.chkAll = New System.Windows.Forms.CheckBox()
        Me.numPrt2 = New System.Windows.Forms.NumericUpDown()
        Me.numPrt1 = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnChk = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkPDF = New System.Windows.Forms.CheckBox()
        Me.chkWord = New System.Windows.Forms.CheckBox()
        Me.chkExcel = New System.Windows.Forms.CheckBox()
        Me.lstProg = New System.Windows.Forms.ListBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnProgClear = New System.Windows.Forms.Button()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numPrt2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numPrt1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(12, 106)
        Me.dgv.Name = "dgv"
        Me.dgv.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.dgv.RowTemplate.Height = 21
        Me.dgv.Size = New System.Drawing.Size(693, 425)
        Me.dgv.TabIndex = 0
        '
        'btnPrt
        '
        Me.btnPrt.Location = New System.Drawing.Point(467, 14)
        Me.btnPrt.Name = "btnPrt"
        Me.btnPrt.Size = New System.Drawing.Size(116, 28)
        Me.btnPrt.TabIndex = 1
        Me.btnPrt.Text = "印刷"
        Me.btnPrt.UseVisualStyleBackColor = True
        '
        'pd
        '
        Me.pd.UseEXDialog = True
        '
        'chkAll
        '
        Me.chkAll.AutoSize = True
        Me.chkAll.Checked = True
        Me.chkAll.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAll.Location = New System.Drawing.Point(12, 84)
        Me.chkAll.Name = "chkAll"
        Me.chkAll.Size = New System.Drawing.Size(60, 16)
        Me.chkAll.TabIndex = 2
        Me.chkAll.Text = "全解除"
        Me.chkAll.UseVisualStyleBackColor = True
        '
        'numPrt2
        '
        Me.numPrt2.Location = New System.Drawing.Point(168, 14)
        Me.numPrt2.Name = "numPrt2"
        Me.numPrt2.Size = New System.Drawing.Size(69, 19)
        Me.numPrt2.TabIndex = 3
        '
        'numPrt1
        '
        Me.numPrt1.Location = New System.Drawing.Point(72, 14)
        Me.numPrt1.Name = "numPrt1"
        Me.numPrt1.Size = New System.Drawing.Size(69, 19)
        Me.numPrt1.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 12)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "月内通番"
        '
        'btnChk
        '
        Me.btnChk.Location = New System.Drawing.Point(243, 9)
        Me.btnChk.Name = "btnChk"
        Me.btnChk.Size = New System.Drawing.Size(63, 27)
        Me.btnChk.TabIndex = 5
        Me.btnChk.Text = "印字候補"
        Me.btnChk.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(147, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(17, 12)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "～"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkPDF)
        Me.GroupBox1.Controls.Add(Me.chkWord)
        Me.GroupBox1.Controls.Add(Me.chkExcel)
        Me.GroupBox1.Location = New System.Drawing.Point(340, 5)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(112, 87)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "ファイル種類"
        '
        'chkPDF
        '
        Me.chkPDF.AutoSize = True
        Me.chkPDF.Checked = True
        Me.chkPDF.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPDF.Location = New System.Drawing.Point(22, 58)
        Me.chkPDF.Name = "chkPDF"
        Me.chkPDF.Size = New System.Drawing.Size(46, 16)
        Me.chkPDF.TabIndex = 2
        Me.chkPDF.Text = "PDF"
        Me.chkPDF.UseVisualStyleBackColor = True
        '
        'chkWord
        '
        Me.chkWord.AutoSize = True
        Me.chkWord.Checked = True
        Me.chkWord.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkWord.Location = New System.Drawing.Point(22, 40)
        Me.chkWord.Name = "chkWord"
        Me.chkWord.Size = New System.Drawing.Size(49, 16)
        Me.chkWord.TabIndex = 1
        Me.chkWord.Text = "Word"
        Me.chkWord.UseVisualStyleBackColor = True
        '
        'chkExcel
        '
        Me.chkExcel.AutoSize = True
        Me.chkExcel.Checked = True
        Me.chkExcel.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkExcel.Location = New System.Drawing.Point(22, 22)
        Me.chkExcel.Name = "chkExcel"
        Me.chkExcel.Size = New System.Drawing.Size(52, 16)
        Me.chkExcel.TabIndex = 0
        Me.chkExcel.Text = "Excel"
        Me.chkExcel.UseVisualStyleBackColor = True
        '
        'lstProg
        '
        Me.lstProg.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstProg.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lstProg.FormattingEnabled = True
        Me.lstProg.ItemHeight = 15
        Me.lstProg.Location = New System.Drawing.Point(711, 106)
        Me.lstProg.Name = "lstProg"
        Me.lstProg.Size = New System.Drawing.Size(176, 424)
        Me.lstProg.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(709, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 12)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "進捗"
        '
        'btnProgClear
        '
        Me.btnProgClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnProgClear.Location = New System.Drawing.Point(802, 67)
        Me.btnProgClear.Name = "btnProgClear"
        Me.btnProgClear.Size = New System.Drawing.Size(85, 33)
        Me.btnProgClear.TabIndex = 10
        Me.btnProgClear.Text = "進捗クリア"
        Me.btnProgClear.UseVisualStyleBackColor = True
        '
        'frmPrt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(905, 543)
        Me.Controls.Add(Me.btnProgClear)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lstProg)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnChk)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.numPrt1)
        Me.Controls.Add(Me.numPrt2)
        Me.Controls.Add(Me.chkAll)
        Me.Controls.Add(Me.btnPrt)
        Me.Controls.Add(Me.dgv)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPrt"
        Me.Text = "印刷"
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numPrt2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numPrt1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents btnPrt As System.Windows.Forms.Button
    Friend WithEvents pd As System.Windows.Forms.PrintDialog
    Friend WithEvents chkAll As System.Windows.Forms.CheckBox
    Friend WithEvents numPrt2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents numPrt1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnChk As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkPDF As System.Windows.Forms.CheckBox
    Friend WithEvents chkWord As System.Windows.Forms.CheckBox
    Friend WithEvents chkExcel As System.Windows.Forms.CheckBox
    Friend WithEvents lstProg As System.Windows.Forms.ListBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnProgClear As System.Windows.Forms.Button
End Class
