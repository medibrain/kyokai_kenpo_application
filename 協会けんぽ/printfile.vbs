'2018-04-19　エクセルをモノクロに設定、他はカラー前提

option explicit
dim arg
dim fso
dim objFile

set arg=Wscript.Arguments
set fso=CreateObject("scripting.FileSystemObject")
dim fn
fn=arg(0)

'サムネイルのときは抜ける
if fso.GetFileName(fn)="Thumbs.db" then wscript.quit

'word,excel印刷
select case fso.GetExtensionName(fn)

	case "docx","doc"
		dim objWord,objDoc
		set objWord=CreateObject("word.application")
		objWord.visible=false
		set objDoc=objWord.Documents.open(arg(0),,true)
		objWord.PrintOut
		
        '//20181116154819 furukawa st ////////////////////////
        '//印刷遅いからsleep解除
		'wscript.sleep 1000
        '//20181116154819 furukawa ed ////////////////////////		

		objDoc.close false
		objWord.quit
		set objWord=nothing
		
	case "xlsx","xls"
		dim objExcel,objWorkbook,objWorkSheet
		set objExcel=CreateObject("Excel.Application")
		set objWorkbook=objExcel.Workbooks.Open(arg(0))
		
		'全シート選択
		objWorkbook.Worksheets.select
		
        'エクセル全シートをモノクロに設定
	    for each objWorkSheet in objWorkbook.Sheets			
            objWorkSheet.PageSetup.BlackAndWhite=true
	    next

'        set objWorkSheet=objWorkbook.Worksheets(2)
 '       objWorkSheet.PageSetup.BlackAndWhite=true
  '      set objWorkSheet=objWorkbook.Worksheets(1)
   '     objWorkSheet.PageSetup.BlackAndWhite=true


		'全シート印刷
		objExcel.ActiveWindow.SelectedSheets.PrintOut
		
        '//20181116154819 furukawa st ////////////////////////
        '//印刷遅いからsleep解除
        'wscript.sleep 1000
        '//20181116154819 furukawa ed ////////////////////////

		objExcel.Workbooks(1).close false
		set objWorkbook=nothing
		objExcel.quit
		set objExcel=nothing

	case "pdf"
		dim objShell
		set objShell=CreateObject("wscript.shell")
		dim strCommand
		objShell.currentdirectory= fso.getParentFolderName(WScript.ScriptFullName)
		strCommand="SumatraPDF.exe -print-to-default """ & arg(0) & """"
		
        '//20181116154819 furukawa st ////////////////////////
        '//印刷遅いからsleep解除
		'wscript.sleep 2000
        '//20181116154819 furukawa ed ////////////////////////
        		
		objShell.run(strCommand)
		set objShell=nothing
		
	case else

		msgbox  fso.GetExtensionName(arg(0))
		msgbox  arg(0) & vbcrlf & "エクセルワードPDF以外"

end select

set fso=nothing
