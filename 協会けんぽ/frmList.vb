﻿Imports System.Data


Public Class frmList
    Dim dtItiran As New DataTable
    Dim cn As New Npgsql.NpgsqlConnection
    Dim da As New Npgsql.NpgsqlDataAdapter

    Dim dv As New System.Data.DataView

    Dim txtList As New List(Of TextBox)

#Region "フォームクロージング"

    Private Sub Form2_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        For Each dr As DataRow In dtItiran.Rows
            If dr.RowState = DataRowState.Modified Then
                If MessageBox.Show("変更が確定されていませんが、閉じますか？", _
                                   Application.ProductName, MessageBoxButtons.YesNo, _
                                   MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = _
                                   Windows.Forms.DialogResult.Yes Then
                    Exit Sub
                Else
                    e.Cancel = True
                    Exit Sub
                End If

            End If
        Next
    End Sub
#End Region

#Region "フォームロード"

    Private Sub Form2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try


            cn.ConnectionString = frmMenu.strConnectionString
            Try
                cn.Open()
            Catch ex As Exception
                MessageBox.Show("DBに接続できませんでした。", _
                                Application.ProductName, _
                                MessageBoxButtons.OK, _
                                MessageBoxIcon.Exclamation)
                Me.Close()
                Exit Sub
            End Try
            
            Dim sbSQL As New System.Text.StringBuilder
            Dim cmd As New Npgsql.NpgsqlCommand
            cmd.Connection = cn

            sbSQL.Remove(0, sbSQL.ToString.Length)

            sbSQL.AppendLine("select ")
            sbSQL.AppendLine("支部コード,")
            sbSQL.AppendLine("依頼年月,")
            sbSQL.AppendLine("月内通番,")
            sbSQL.AppendLine("委託区分,")
            sbSQL.AppendLine("種別,")
            sbSQL.AppendLine("項番,")
            sbSQL.AppendLine("被保険者記号,")
            sbSQL.AppendLine("被保険者番号,")
            sbSQL.AppendLine("国名,")
            sbSQL.AppendLine("外国語,")
            sbSQL.AppendLine("ページ数申請書含,")
            sbSQL.AppendLine("本人家族,")

            sbSQL.AppendLine("作成者,")
            sbSQL.AppendLine("レセ要不要,")
            sbSQL.AppendLine("作成期限,")
            sbSQL.AppendLine("医科歯科,")
            sbSQL.AppendLine("完了日,")
            sbSQL.AppendLine("振分け ")

            sbSQL.AppendLine("申請額_円,")
            sbSQL.AppendLine("翻訳,")
            sbSQL.AppendLine("レセ作成算定状況,")
            sbSQL.AppendLine("算定対象件数,")
            sbSQL.AppendLine("納品日,")
            sbSQL.AppendLine("支給不支給返戻支給不支給,")
            sbSQL.AppendLine("決定金額,")
            sbSQL.AppendLine("査定額,")
            sbSQL.AppendLine("査定不支給理由,")
            sbSQL.AppendLine("海外拠点の有無 ")

            sbSQL.AppendLine(" from manage ")
            sbSQL.AppendLine(" order by cast(依頼年月 as int),no,月内通番")

            cmd.CommandText = sbSQL.ToString()

            da.SelectCommand = cmd
            da.Fill(dtItiran)




            Dim bs As New BindingSource

            bs.DataSource = dtItiran

            Me.dg.DataSource = bs

            '選択用チェックボックス列を追加
            If Not dg.Columns.Contains("選択") Then
                Dim cbc As New DataGridViewCheckBoxColumn
                cbc.Name = "選択"
                dg.Columns.Insert(0, cbc)
            End If

            '固定列
            dg.Columns("項番").Frozen = True

            '和暦
            Dim cul As New System.Globalization.CultureInfo("ja-JP", True)
            cul.DateTimeFormat.Calendar = New System.Globalization.JapaneseCalendar
            Dim dtToday As String = Today.AddMonths(-1).ToString("yyMM", cul)


            'コントロールの配置間隔
            Dim y As Integer = 20

            'フィルタ用入力箇所作成
            For Each cl As DataColumn In dtItiran.Columns
                Dim lbl As New Label
                lbl.Name = "lbl_" & cl.ColumnName
                lbl.Text = cl.ColumnName
                lbl.Location = New Point(5, y)

                Dim txt As New TextBox
                txt.Name = "txt_" & cl.ColumnName
                txt.Location = New Point(lbl.Width + 5, y)
                txt.Width = 120 'テキストボックスの幅

                txt.AutoCompleteMode = AutoCompleteMode.SuggestAppend
                txt.AutoCompleteSource = AutoCompleteSource.CustomSource

                txt.AutoCompleteCustomSource.Clear()
                For Each dr As DataRow In dtItiran.Rows
                    If IsDBNull(dr(cl.ColumnName)) Then Continue For
                    txt.AutoCompleteCustomSource.Add(dr(cl.ColumnName))
                Next

                If txt.Name = "txt_依頼年月" Then txt.Text = dtToday

                txtList.Add(txt)


                Me.gb.Controls.Add(lbl)
                Me.gb.Controls.Add(txt)
                y += lbl.Height
            Next

            Me.gb.Height = y + 5

            'コンボ作成
            For Each cl As DataColumn In dtItiran.Columns
                cmbCol.Items.Add(cl.ColumnName)
            Next



            '最初からフィルタ適用にする
            btnFilter_Click(sender, e)

        Catch ex As Exception
            MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
#End Region

#Region "フィルタボタン"

    Private Sub btnFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        If Not ChkIraiYm() Then Exit Sub

        dv = dtItiran.AsDataView

        Dim strfilter As String = String.Empty

        For Each ctl As Control In gb.Controls
            'テキストボックス
            If ctl.GetType.ToString() = "System.Windows.Forms.TextBox" Then
                '中身がある場合
                If ctl.Text.Trim <> String.Empty Then

                    '同名のラベルを取得
                    Dim lbl As Control() = gb.Controls.Find("lbl_" & ctl.Name.Substring(4, ctl.Name.Length - 4), False)
                    'なかったら飛ばす
                    If lbl.Length = 0 Then Continue For

                    'フィールド名はラベル名から取得
                    Dim fldname = CType(lbl(0), Label).Name.Substring(4, lbl(0).Name.Length - 4)

                    'フィルタ文字列の作成
                    If strfilter.Length > 0 Then strfilter &= " and "
                    strfilter &= fldname & "='" & ctl.Text & "'"

                End If
            End If
        Next



        dv.RowFilter = strfilter
        dv.AllowEdit = True
        dg.DataSource = dv

    End Sub
#End Region

#Region "依頼年月の必須入力チェック"

    ''' <summary>
    ''' 依頼年月の必須入力チェック
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>これがないと全部更新されるため</remarks>
    Private Function ChkIraiYm() As Boolean
        For Each txt As TextBox In txtList
            If txt.Name = "txt_依頼年月" Then
                If txt.Text = String.Empty Then
                    MessageBox.Show("依頼年月は必須入力です", _
                                    Application.ProductName, _
                                    MessageBoxButtons.OK, _
                                    MessageBoxIcon.Exclamation)

                    Return False

                End If
            End If
        Next
        Return True

    End Function
#End Region

#Region "更新ボタン"

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            If Not ChkIraiYm() Then Exit Sub

            Dim cb As New Npgsql.NpgsqlCommandBuilder(da)
            da.Update(dtItiran)

            MessageBox.Show("更新しました", Application.ProductName, _
                            MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            da.Dispose()


        End Try
    End Sub
#End Region

#Region "一括入力ボタン"

    Private Sub btnAllUpd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAllUpd.Click
        If txtInput.Text = String.Empty Then Exit Sub

        If Not ChkIraiYm() Then Exit Sub

        If cmbCol.Text = "納品日" Then
            If txtInput.Text.Length > 6 Then
                MessageBox.Show("納品日はyymmddで入力してください", _
                                Application.ProductName, MessageBoxButtons.OK, _
                                MessageBoxIcon.Exclamation)
                txtInput.Text = String.Empty
                Exit Sub
            End If
        End If

        For r As Integer = 0 To dg.Rows.Count - 1
            If dg.Rows(r).Cells(0).Value = True Then
                Dim dr As DataGridViewRow = dg.Rows(r)
                Dim strcol As String = cmbCol.Text
                Dim cs As New DataGridViewCellStyle
                cs.BackColor = Color.Salmon
                dr.DefaultCellStyle = cs

                dr.Cells(strcol).Value = txtInput.Text.ToString()
            End If
        Next


        'If dg.SelectedRows.Count > 0 Then
        '    For Each dr As DataGridViewRow In dg.SelectedRows
        '        dr.Cells(cmbCol.SelectedIndex).Value = txtInput.Text.ToString()
        '    Next
        'Else
        '    For Each dr As DataGridViewRow In dg.Rows
        '        dr.Cells(cmbCol.SelectedIndex + 1).Value = txtInput.Text.ToString()
        '    Next
        'End If

    End Sub

#End Region

#Region "一括入力コンボ変更時"

    Private Sub cmbCol_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCol.SelectionChangeCommitted
        dg.FirstDisplayedScrollingColumnIndex = cmbCol.SelectedIndex

    End Sub
#End Region

  
#Region "datagridview入力チェック"

    Private Sub dg_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dg.CellValidating
        If dg.Columns(e.ColumnIndex).Name = "納品日" Then
            If e.FormattedValue.ToString.Length > 6 Then
                MsgBox("yymmdd")
                e.Cancel = True
            End If
        End If

    End Sub
#End Region

    Private Sub dg_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dg.SelectionChanged
        If dg.SelectedRows.Count = 0 Then Exit Sub


    End Sub
End Class