option explicit
on error resume next

dim objWord,objDoc
dim args
dim fso
dim strFullFilePath

dim strKimitusei'【機密性３】はVBから取得する
Const wdSeekCurrentPageHeader=9
Const wdAlignParagraphLeft=0

set args=Wscript.Arguments
set fso=CreateObject("scripting.FileSystemObject")

strKimitusei=split(args(0),",")(1)
strFullFilePath=split(args(0),",")(0)

dim strExtension
strExtension=fso.GetExtensionName(strFullFilePath)'ファイルの拡張子を取得

'wordでない場合抜ける
if strExtension <>"docx" then 
    Wscript.Quit
    wscript.sleep 1500
end if

'一時ファイルの場合抜ける
'if instr(args,"~$")>0 then
 '   Wscript.Quit
  '  wscript.sleep 100
'end if


set objWord=CreateObject("word.application")
objWord.visible=false
set objDoc=objWord.Documents.open(strFullFilePath,,false)

objDoc.ActiveWindow.ActivePane.View.SeekView=wdSeekCurrentPageHeader

'上から下の文字列にする
'【機密性3】項番1429113421B　　　　　　　　　　　　　　　　　　　　中国語　→　日本語
'【機密性3】中国語　→　日本語　　　　　　　　　　　　　　　　　　　　項番1429113421B

'上から下の文字列にする
'中国語　→　日本語
'【機密性3】　　　　　　　　　　　　　　　　　　　　項番1429113421B
'  中国語　→　日本語


'wordのヘッダ文字列を取得
dim strHeader
strHeader=objDoc.Sections(1).headers(1).range.text


'言語文字列
dim strLanguage

strLanguage=trim(strHeader)'言語をトリム
strLanguage=replace(strLanguage,vbcr,"")'改行を削除




'言語のみ正規表現で抜きだし
Dim objReg
Set objReg = CreateObject("vbscript.regexp")

'漢字、カタカナ、ひらがなと「日本語」を抜き出し
'10文字ぐらい
'objReg.Pattern = ".{1,5}語.+日本語"
'objReg.Pattern = "[ァ-ヴ][ァ-ヴー・]{1,6}語.+日本語"
objReg.Pattern = "[一-龠ぁ-んーァ-ヶァ-ヴー・]{1,6}語.+日本語"

objReg.IgnoreCase = True
objReg.Global = True

Dim matchs
Dim s
Set matchs = objReg.Execute(strHeader)

For Each s In matchs
    strLanguage= trim(s)
Next
dim n
n=instr(0,strLanguage,"】")
strLanguage=trim(replace(strLanguage,"】",""))


'純粋なファイル名だけを抽出
dim fn
fn=replace(fso.GetFileName(strFullFilePath),"." & strExtension,"")

'赤線を削除
if instr(1,fn,"赤線")>0 then fn=replace(fn,"赤線","")

'ヘッダ文字列を設定
'objDoc.Sections(1).headers(1).range.text= _
'"【機密性3】" &  space(60) & fn & vbcrlf & space(3) & strLanguage

'【機密性3】ファイル名 改行 言語
'objDoc.Sections(1).headers(1).range.text=""
'objDoc.Sections(1).headers(1).range.text= _
'strKimitusei & fn & vbcrlf & space(3) & strLanguage



'objDoc.Sections(1).headers(1).range.ParagraphFormat.Alignment=wdAlignParagraphLeft
'objDoc.Sections(1).headers(1).range.font.name="ＭＳ 明朝"
'objDoc.Sections(1).headers(1).range.font.size="10.5"

with objDoc.Sections(1).headers(1).range

    '【機密性3】ファイル名拡張子抜き 改行 言語
    .text=""
    '.text= strKimitusei & fn & vbcrlf & space(3) & strLanguage

    '【機密性3】言語 ファイル名拡張子抜き
    .text= strKimitusei & space(1) & strLanguage & fn 

    .ParagraphFormat.Alignment=wdAlignParagraphLeft
    .font.name="ＭＳ 明朝"
    .font.size="10.5"
end with

'objWord.Selection.MoveRight 1,len(strKimitusei)
objWord.Selection.MoveRight 1,len(strKimitusei & space(1) & strLanguage)
objWord.WordBasic.InsertAlignmentTab 2,0,0



'objDoc.Sections(1).headers(1).range.text="サンプル語→日本語"


objDoc.close true
set objDoc=nothing

wscript.sleep 1500

objWord.Quit
set objWord=nothing

'//20180718095347 furukawa st ////////////////////////
'//残っているwinword.exeを強制削除
call ProcessKill
'//20180718095347 furukawa ed ////////////////////////

'エラーになったファイル名を返す
dim errfn

if err.number<>0 then

    set errfn=fn

    objDoc.close false
    set objDoc=nothing

    wscript.sleep 1500

    objWord.Quit
    set objWord=nothing

end if

wscript.quit(errfn)

'//20180718095347 furukawa st ////////////////////////
'//残っているwinword.exeを強制削除
sub ProcessKill
    On Error Resume Next
 
    Dim strProcName ' 終了するプロセス名
    Dim objProcList ' プロセス一覧
    Dim objProcess ' プロセス情報
    Dim lngKillNum ' 終了したプロセス数
 
    strProcName = "winword.exe"
    lngKillNum = 0
 
    Set objProcList = GetObject("winmgmts:").InstancesOf("win32_process")
    For Each objProcess In objProcList
        If LCase(objProcess.Name) = strProcName Then
            objProcess.Terminate
            If Err.Number = 0 Then
                lngKillNum = lngKillNum + 1
            Else
                'WScript.Echo "エラー: " &amp; Err.Description
            End If
        End If
    Next
'    If lngKillNum &gt; 0 Then
'        WScript.Echo strProcName &amp; " を " &amp; lngKillNum &amp; " 個強制終了しました。"
'    Else
'        WScript.Echo strProcName &amp; " が見つかりませんでした。"
'    End If
 
    Set objProcList = Nothing
end sub
'//20180718095347 furukawa ed ////////////////////////
