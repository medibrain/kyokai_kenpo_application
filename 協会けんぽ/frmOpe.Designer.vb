﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOpe
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
    'Windows フォーム デザイナを使用して変更できます。  
    'コード エディタを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOpe))
        Me.btnExcelSelect = New System.Windows.Forms.Button()
        Me.lstSheet = New System.Windows.Forms.ListBox()
        Me.btnGetData = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnFolderPlace = New System.Windows.Forms.Button()
        Me.lblFolderCreatePath = New System.Windows.Forms.Label()
        Me.btnCreateFolder_FileMove = New System.Windows.Forms.Button()
        Me.btnRecipt = New System.Windows.Forms.Button()
        Me.lblReceiptFilesPath = New System.Windows.Forms.Label()
        Me.btnImport2DB = New System.Windows.Forms.Button()
        Me.btnItiran = New System.Windows.Forms.Button()
        Me.rb_set = New System.Windows.Forms.RadioButton()
        Me.rb_koban = New System.Windows.Forms.RadioButton()
        Me.lblCnt = New System.Windows.Forms.Label()
        Me.rb_itaku2 = New System.Windows.Forms.RadioButton()
        Me.rb_itaku1 = New System.Windows.Forms.RadioButton()
        Me.gbItaku = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnCountFiles = New System.Windows.Forms.Button()
        Me.lblManageFile = New System.Windows.Forms.Label()
        Me.rb_FileCount = New System.Windows.Forms.RadioButton()
        Me.gb = New System.Windows.Forms.GroupBox()
        Me.rb_Dummy = New System.Windows.Forms.RadioButton()
        Me.rb_printkoban = New System.Windows.Forms.RadioButton()
        Me.rb_FileCountSet = New System.Windows.Forms.RadioButton()
        Me.rb_createSagyo = New System.Windows.Forms.RadioButton()
        Me.rb_NohinDtUPD = New System.Windows.Forms.RadioButton()
        Me.rb_FileFolderName = New System.Windows.Forms.RadioButton()
        Me.rb_Header = New System.Windows.Forms.RadioButton()
        Me.btnCreateSagyo = New System.Windows.Forms.Button()
        Me.lblRecCnt = New System.Windows.Forms.Label()
        Me.numHeaderRow = New System.Windows.Forms.NumericUpDown()
        Me.numDataRow = New System.Windows.Forms.NumericUpDown()
        Me.btnPrt = New System.Windows.Forms.Button()
        Me.cmbKaisu = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbItaku = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnSagyolistOut = New System.Windows.Forms.Button()
        Me.btnHeader = New System.Windows.Forms.Button()
        Me.bg1 = New System.ComponentModel.BackgroundWorker()
        Me.pb1 = New System.Windows.Forms.ProgressBar()
        Me.bg2 = New System.ComponentModel.BackgroundWorker()
        Me.pb2 = New System.Windows.Forms.ProgressBar()
        Me.btnRename = New System.Windows.Forms.Button()
        Me.bg3 = New System.ComponentModel.BackgroundWorker()
        Me.bg4 = New System.ComponentModel.BackgroundWorker()
        Me.pnlHeader = New System.Windows.Forms.Panel()
        Me.chkXls = New System.Windows.Forms.CheckBox()
        Me.chkDoc = New System.Windows.Forms.CheckBox()
        Me.pnlFF = New System.Windows.Forms.Panel()
        Me.gbString = New System.Windows.Forms.GroupBox()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.btnColor = New System.Windows.Forms.Button()
        Me.pnlSheet = New System.Windows.Forms.Panel()
        Me.pnlSagyohyo = New System.Windows.Forms.Panel()
        Me.lblDB = New System.Windows.Forms.Label()
        Me.ss = New System.Windows.Forms.StatusStrip()
        Me.tssl = New System.Windows.Forms.ToolStripStatusLabel()
        Me.gbItaku.SuspendLayout()
        Me.gb.SuspendLayout()
        CType(Me.numHeaderRow, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numDataRow, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHeader.SuspendLayout()
        Me.pnlFF.SuspendLayout()
        Me.gbString.SuspendLayout()
        Me.pnlSheet.SuspendLayout()
        Me.pnlSagyohyo.SuspendLayout()
        Me.ss.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExcelSelect
        '
        Me.btnExcelSelect.BackColor = System.Drawing.SystemColors.Control
        Me.btnExcelSelect.Enabled = False
        Me.btnExcelSelect.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnExcelSelect.Location = New System.Drawing.Point(314, 20)
        Me.btnExcelSelect.Name = "btnExcelSelect"
        Me.btnExcelSelect.Size = New System.Drawing.Size(252, 32)
        Me.btnExcelSelect.TabIndex = 1
        Me.btnExcelSelect.Text = "①一覧表Excel選択"
        Me.btnExcelSelect.UseVisualStyleBackColor = False
        '
        'lstSheet
        '
        Me.lstSheet.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lstSheet.FormattingEnabled = True
        Me.lstSheet.ItemHeight = 15
        Me.lstSheet.Location = New System.Drawing.Point(11, 26)
        Me.lstSheet.Name = "lstSheet"
        Me.lstSheet.Size = New System.Drawing.Size(335, 109)
        Me.lstSheet.TabIndex = 1
        '
        'btnGetData
        '
        Me.btnGetData.BackColor = System.Drawing.SystemColors.Control
        Me.btnGetData.Enabled = False
        Me.btnGetData.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnGetData.Location = New System.Drawing.Point(314, 207)
        Me.btnGetData.Name = "btnGetData"
        Me.btnGetData.Size = New System.Drawing.Size(252, 30)
        Me.btnGetData.TabIndex = 4
        Me.btnGetData.Text = "③データ取得"
        Me.btnGetData.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label1.Location = New System.Drawing.Point(387, 46)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 15)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "ヘッダ行"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label2.Location = New System.Drawing.Point(387, 87)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(103, 15)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "データ開始行"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label3.Location = New System.Drawing.Point(8, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(119, 15)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "②シート名選択"
        '
        'btnFolderPlace
        '
        Me.btnFolderPlace.BackColor = System.Drawing.SystemColors.Control
        Me.btnFolderPlace.Enabled = False
        Me.btnFolderPlace.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnFolderPlace.Location = New System.Drawing.Point(314, 242)
        Me.btnFolderPlace.Name = "btnFolderPlace"
        Me.btnFolderPlace.Size = New System.Drawing.Size(252, 30)
        Me.btnFolderPlace.TabIndex = 5
        Me.btnFolderPlace.Text = "④フォルダ作成する場所"
        Me.btnFolderPlace.UseVisualStyleBackColor = False
        '
        'lblFolderCreatePath
        '
        Me.lblFolderCreatePath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFolderCreatePath.BackColor = System.Drawing.SystemColors.Control
        Me.lblFolderCreatePath.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblFolderCreatePath.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFolderCreatePath.Location = New System.Drawing.Point(572, 242)
        Me.lblFolderCreatePath.Name = "lblFolderCreatePath"
        Me.lblFolderCreatePath.Size = New System.Drawing.Size(586, 30)
        Me.lblFolderCreatePath.TabIndex = 8
        '
        'btnCreateFolder_FileMove
        '
        Me.btnCreateFolder_FileMove.BackColor = System.Drawing.SystemColors.Control
        Me.btnCreateFolder_FileMove.Enabled = False
        Me.btnCreateFolder_FileMove.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnCreateFolder_FileMove.Location = New System.Drawing.Point(312, 404)
        Me.btnCreateFolder_FileMove.Name = "btnCreateFolder_FileMove"
        Me.btnCreateFolder_FileMove.Size = New System.Drawing.Size(335, 37)
        Me.btnCreateFolder_FileMove.TabIndex = 8
        Me.btnCreateFolder_FileMove.Text = "⑥作成してファイルを振り分ける"
        Me.btnCreateFolder_FileMove.UseVisualStyleBackColor = False
        '
        'btnRecipt
        '
        Me.btnRecipt.BackColor = System.Drawing.SystemColors.Control
        Me.btnRecipt.Enabled = False
        Me.btnRecipt.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnRecipt.Location = New System.Drawing.Point(314, 278)
        Me.btnRecipt.Name = "btnRecipt"
        Me.btnRecipt.Size = New System.Drawing.Size(252, 30)
        Me.btnRecipt.TabIndex = 6
        Me.btnRecipt.Text = "⑤レセファイルの場所"
        Me.btnRecipt.UseVisualStyleBackColor = False
        '
        'lblReceiptFilesPath
        '
        Me.lblReceiptFilesPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblReceiptFilesPath.BackColor = System.Drawing.SystemColors.Control
        Me.lblReceiptFilesPath.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblReceiptFilesPath.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblReceiptFilesPath.Location = New System.Drawing.Point(572, 278)
        Me.lblReceiptFilesPath.Name = "lblReceiptFilesPath"
        Me.lblReceiptFilesPath.Size = New System.Drawing.Size(586, 30)
        Me.lblReceiptFilesPath.TabIndex = 8
        '
        'btnImport2DB
        '
        Me.btnImport2DB.BackColor = System.Drawing.SystemColors.Control
        Me.btnImport2DB.Enabled = False
        Me.btnImport2DB.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnImport2DB.Location = New System.Drawing.Point(314, 514)
        Me.btnImport2DB.Name = "btnImport2DB"
        Me.btnImport2DB.Size = New System.Drawing.Size(192, 39)
        Me.btnImport2DB.TabIndex = 10
        Me.btnImport2DB.Text = "DB登録"
        Me.btnImport2DB.UseVisualStyleBackColor = False
        '
        'btnItiran
        '
        Me.btnItiran.Location = New System.Drawing.Point(73, 551)
        Me.btnItiran.Name = "btnItiran"
        Me.btnItiran.Size = New System.Drawing.Size(77, 27)
        Me.btnItiran.TabIndex = 10
        Me.btnItiran.Text = "管理表"
        Me.btnItiran.UseVisualStyleBackColor = True
        Me.btnItiran.Visible = False
        '
        'rb_set
        '
        Me.rb_set.AutoSize = True
        Me.rb_set.Location = New System.Drawing.Point(17, 77)
        Me.rb_set.Name = "rb_set"
        Me.rb_set.Size = New System.Drawing.Size(193, 27)
        Me.rb_set.TabIndex = 13
        Me.rb_set.Text = "セット番号フォルダ生成"
        Me.rb_set.UseVisualStyleBackColor = True
        '
        'rb_koban
        '
        Me.rb_koban.AutoSize = True
        Me.rb_koban.Location = New System.Drawing.Point(17, 369)
        Me.rb_koban.Name = "rb_koban"
        Me.rb_koban.Size = New System.Drawing.Size(148, 27)
        Me.rb_koban.TabIndex = 12
        Me.rb_koban.Text = "項番フォルダ生成"
        Me.rb_koban.UseVisualStyleBackColor = True
        '
        'lblCnt
        '
        Me.lblCnt.BackColor = System.Drawing.SystemColors.Control
        Me.lblCnt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCnt.Location = New System.Drawing.Point(572, 207)
        Me.lblCnt.Name = "lblCnt"
        Me.lblCnt.Size = New System.Drawing.Size(225, 30)
        Me.lblCnt.TabIndex = 14
        '
        'rb_itaku2
        '
        Me.rb_itaku2.AutoSize = True
        Me.rb_itaku2.Location = New System.Drawing.Point(202, 30)
        Me.rb_itaku2.Name = "rb_itaku2"
        Me.rb_itaku2.Size = New System.Drawing.Size(127, 27)
        Me.rb_itaku2.TabIndex = 16
        Me.rb_itaku2.TabStop = True
        Me.rb_itaku2.Text = "2（翻訳不要）"
        Me.rb_itaku2.UseVisualStyleBackColor = True
        '
        'rb_itaku1
        '
        Me.rb_itaku1.AutoSize = True
        Me.rb_itaku1.Location = New System.Drawing.Point(47, 30)
        Me.rb_itaku1.Name = "rb_itaku1"
        Me.rb_itaku1.Size = New System.Drawing.Size(112, 27)
        Me.rb_itaku1.TabIndex = 15
        Me.rb_itaku1.TabStop = True
        Me.rb_itaku1.Text = "1（翻訳要）"
        Me.rb_itaku1.UseVisualStyleBackColor = True
        '
        'gbItaku
        '
        Me.gbItaku.BackColor = System.Drawing.SystemColors.Control
        Me.gbItaku.Controls.Add(Me.rb_itaku2)
        Me.gbItaku.Controls.Add(Me.rb_itaku1)
        Me.gbItaku.Enabled = False
        Me.gbItaku.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.gbItaku.Location = New System.Drawing.Point(312, 325)
        Me.gbItaku.Name = "gbItaku"
        Me.gbItaku.Size = New System.Drawing.Size(355, 64)
        Me.gbItaku.TabIndex = 7
        Me.gbItaku.TabStop = False
        Me.gbItaku.Text = "委託区分(セット番号の場合のみ）"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(62, 518)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 27)
        Me.Button1.TabIndex = 18
        Me.Button1.Text = "合計点数取得"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'btnCountFiles
        '
        Me.btnCountFiles.BackColor = System.Drawing.SystemColors.Control
        Me.btnCountFiles.Enabled = False
        Me.btnCountFiles.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnCountFiles.Location = New System.Drawing.Point(312, 445)
        Me.btnCountFiles.Name = "btnCountFiles"
        Me.btnCountFiles.Size = New System.Drawing.Size(335, 33)
        Me.btnCountFiles.TabIndex = 9
        Me.btnCountFiles.Text = "⑦ファイル数カウントして管理表に入れる"
        Me.btnCountFiles.UseVisualStyleBackColor = False
        '
        'lblManageFile
        '
        Me.lblManageFile.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblManageFile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblManageFile.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblManageFile.Location = New System.Drawing.Point(572, 20)
        Me.lblManageFile.Name = "lblManageFile"
        Me.lblManageFile.Size = New System.Drawing.Size(586, 32)
        Me.lblManageFile.TabIndex = 20
        '
        'rb_FileCount
        '
        Me.rb_FileCount.AutoSize = True
        Me.rb_FileCount.Location = New System.Drawing.Point(17, 227)
        Me.rb_FileCount.Name = "rb_FileCount"
        Me.rb_FileCount.Size = New System.Drawing.Size(238, 50)
        Me.rb_FileCount.TabIndex = 21
        Me.rb_FileCount.Text = "作業表へのファイル数カウント" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(項番フォルダ)"
        Me.rb_FileCount.UseVisualStyleBackColor = True
        '
        'gb
        '
        Me.gb.Controls.Add(Me.rb_Dummy)
        Me.gb.Controls.Add(Me.rb_printkoban)
        Me.gb.Controls.Add(Me.rb_FileCountSet)
        Me.gb.Controls.Add(Me.rb_createSagyo)
        Me.gb.Controls.Add(Me.rb_NohinDtUPD)
        Me.gb.Controls.Add(Me.rb_FileFolderName)
        Me.gb.Controls.Add(Me.rb_Header)
        Me.gb.Controls.Add(Me.rb_koban)
        Me.gb.Controls.Add(Me.rb_FileCount)
        Me.gb.Controls.Add(Me.rb_set)
        Me.gb.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.gb.Location = New System.Drawing.Point(14, 12)
        Me.gb.Name = "gb"
        Me.gb.Size = New System.Drawing.Size(273, 483)
        Me.gb.TabIndex = 22
        Me.gb.TabStop = False
        Me.gb.Text = "処理選択"
        '
        'rb_Dummy
        '
        Me.rb_Dummy.AutoSize = True
        Me.rb_Dummy.Checked = True
        Me.rb_Dummy.Location = New System.Drawing.Point(48, 447)
        Me.rb_Dummy.Name = "rb_Dummy"
        Me.rb_Dummy.Size = New System.Drawing.Size(82, 27)
        Me.rb_Dummy.TabIndex = 26
        Me.rb_Dummy.TabStop = True
        Me.rb_Dummy.Text = "dummy"
        Me.rb_Dummy.UseVisualStyleBackColor = True
        Me.rb_Dummy.Visible = False
        '
        'rb_printkoban
        '
        Me.rb_printkoban.AutoSize = True
        Me.rb_printkoban.Location = New System.Drawing.Point(17, 285)
        Me.rb_printkoban.Name = "rb_printkoban"
        Me.rb_printkoban.Size = New System.Drawing.Size(148, 27)
        Me.rb_printkoban.TabIndex = 25
        Me.rb_printkoban.Text = "項番フォルダ印刷"
        Me.rb_printkoban.UseVisualStyleBackColor = True
        '
        'rb_FileCountSet
        '
        Me.rb_FileCountSet.AutoSize = True
        Me.rb_FileCountSet.Location = New System.Drawing.Point(17, 127)
        Me.rb_FileCountSet.Name = "rb_FileCountSet"
        Me.rb_FileCountSet.Size = New System.Drawing.Size(238, 50)
        Me.rb_FileCountSet.TabIndex = 24
        Me.rb_FileCountSet.Text = "作業表へのファイル数カウント" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(セット番号フォルダ)"
        Me.rb_FileCountSet.UseVisualStyleBackColor = True
        '
        'rb_createSagyo
        '
        Me.rb_createSagyo.AutoSize = True
        Me.rb_createSagyo.Location = New System.Drawing.Point(17, 39)
        Me.rb_createSagyo.Name = "rb_createSagyo"
        Me.rb_createSagyo.Size = New System.Drawing.Size(118, 27)
        Me.rb_createSagyo.TabIndex = 23
        Me.rb_createSagyo.Text = "作業表の作成"
        Me.rb_createSagyo.UseVisualStyleBackColor = True
        '
        'rb_NohinDtUPD
        '
        Me.rb_NohinDtUPD.AutoSize = True
        Me.rb_NohinDtUPD.Location = New System.Drawing.Point(17, 410)
        Me.rb_NohinDtUPD.Name = "rb_NohinDtUPD"
        Me.rb_NohinDtUPD.Size = New System.Drawing.Size(124, 27)
        Me.rb_NohinDtUPD.TabIndex = 12
        Me.rb_NohinDtUPD.Text = "DB納品日更新"
        Me.rb_NohinDtUPD.UseVisualStyleBackColor = True
        '
        'rb_FileFolderName
        '
        Me.rb_FileFolderName.AutoSize = True
        Me.rb_FileFolderName.Location = New System.Drawing.Point(17, 313)
        Me.rb_FileFolderName.Name = "rb_FileFolderName"
        Me.rb_FileFolderName.Size = New System.Drawing.Size(133, 50)
        Me.rb_FileFolderName.TabIndex = 12
        Me.rb_FileFolderName.Text = "ファイル/" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "フォルダ名設定"
        Me.rb_FileFolderName.UseVisualStyleBackColor = True
        '
        'rb_Header
        '
        Me.rb_Header.AutoSize = True
        Me.rb_Header.Location = New System.Drawing.Point(17, 192)
        Me.rb_Header.Name = "rb_Header"
        Me.rb_Header.Size = New System.Drawing.Size(230, 27)
        Me.rb_Header.TabIndex = 12
        Me.rb_Header.Text = "フォーマット設定/ヘッダ設定"
        Me.rb_Header.UseVisualStyleBackColor = True
        '
        'btnCreateSagyo
        '
        Me.btnCreateSagyo.BackColor = System.Drawing.SystemColors.Control
        Me.btnCreateSagyo.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnCreateSagyo.Location = New System.Drawing.Point(3, 3)
        Me.btnCreateSagyo.Name = "btnCreateSagyo"
        Me.btnCreateSagyo.Size = New System.Drawing.Size(262, 35)
        Me.btnCreateSagyo.TabIndex = 11
        Me.btnCreateSagyo.Text = "作業表作成(DB)"
        Me.btnCreateSagyo.UseVisualStyleBackColor = False
        '
        'lblRecCnt
        '
        Me.lblRecCnt.BackColor = System.Drawing.SystemColors.Control
        Me.lblRecCnt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblRecCnt.Location = New System.Drawing.Point(516, 514)
        Me.lblRecCnt.Name = "lblRecCnt"
        Me.lblRecCnt.Size = New System.Drawing.Size(151, 39)
        Me.lblRecCnt.TabIndex = 14
        '
        'numHeaderRow
        '
        Me.numHeaderRow.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.numHeaderRow.Location = New System.Drawing.Point(499, 44)
        Me.numHeaderRow.Name = "numHeaderRow"
        Me.numHeaderRow.Size = New System.Drawing.Size(65, 22)
        Me.numHeaderRow.TabIndex = 2
        Me.numHeaderRow.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.numHeaderRow.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'numDataRow
        '
        Me.numDataRow.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.numDataRow.Location = New System.Drawing.Point(499, 85)
        Me.numDataRow.Name = "numDataRow"
        Me.numDataRow.Size = New System.Drawing.Size(65, 22)
        Me.numDataRow.TabIndex = 3
        Me.numDataRow.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.numDataRow.Value = New Decimal(New Integer() {6, 0, 0, 0})
        '
        'btnPrt
        '
        Me.btnPrt.BackColor = System.Drawing.SystemColors.Control
        Me.btnPrt.Enabled = False
        Me.btnPrt.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnPrt.Location = New System.Drawing.Point(314, 708)
        Me.btnPrt.Name = "btnPrt"
        Me.btnPrt.Size = New System.Drawing.Size(202, 35)
        Me.btnPrt.TabIndex = 23
        Me.btnPrt.Text = "印刷画面"
        Me.btnPrt.UseVisualStyleBackColor = False
        '
        'cmbKaisu
        '
        Me.cmbKaisu.FormattingEnabled = True
        Me.cmbKaisu.Location = New System.Drawing.Point(344, 50)
        Me.cmbKaisu.Name = "cmbKaisu"
        Me.cmbKaisu.Size = New System.Drawing.Size(139, 31)
        Me.cmbKaisu.TabIndex = 24
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(271, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 23)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "回数"
        '
        'cmbItaku
        '
        Me.cmbItaku.FormattingEnabled = True
        Me.cmbItaku.Location = New System.Drawing.Point(344, 85)
        Me.cmbItaku.Name = "cmbItaku"
        Me.cmbItaku.Size = New System.Drawing.Size(139, 31)
        Me.cmbItaku.TabIndex = 24
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(271, 88)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(70, 23)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "委託区分"
        '
        'btnSagyolistOut
        '
        Me.btnSagyolistOut.BackColor = System.Drawing.SystemColors.Control
        Me.btnSagyolistOut.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnSagyolistOut.Location = New System.Drawing.Point(3, 85)
        Me.btnSagyolistOut.Name = "btnSagyolistOut"
        Me.btnSagyolistOut.Size = New System.Drawing.Size(262, 35)
        Me.btnSagyolistOut.TabIndex = 11
        Me.btnSagyolistOut.Text = "Excelへ作業表出力"
        Me.btnSagyolistOut.UseVisualStyleBackColor = False
        '
        'btnHeader
        '
        Me.btnHeader.BackColor = System.Drawing.SystemColors.Control
        Me.btnHeader.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnHeader.Location = New System.Drawing.Point(8, 17)
        Me.btnHeader.Name = "btnHeader"
        Me.btnHeader.Size = New System.Drawing.Size(109, 57)
        Me.btnHeader.TabIndex = 26
        Me.btnHeader.Text = "フォーマット" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "ヘッダ設定"
        Me.btnHeader.UseVisualStyleBackColor = False
        '
        'bg1
        '
        Me.bg1.WorkerReportsProgress = True
        '
        'pb1
        '
        Me.pb1.Location = New System.Drawing.Point(201, 19)
        Me.pb1.Name = "pb1"
        Me.pb1.Size = New System.Drawing.Size(169, 21)
        Me.pb1.TabIndex = 27
        '
        'bg2
        '
        Me.bg2.WorkerReportsProgress = True
        '
        'pb2
        '
        Me.pb2.Location = New System.Drawing.Point(201, 51)
        Me.pb2.Name = "pb2"
        Me.pb2.Size = New System.Drawing.Size(169, 21)
        Me.pb2.TabIndex = 28
        '
        'btnRename
        '
        Me.btnRename.BackColor = System.Drawing.SystemColors.Control
        Me.btnRename.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnRename.Location = New System.Drawing.Point(10, 20)
        Me.btnRename.Name = "btnRename"
        Me.btnRename.Size = New System.Drawing.Size(128, 37)
        Me.btnRename.TabIndex = 29
        Me.btnRename.Text = "ファイル" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "フォルダ名設定"
        Me.btnRename.UseVisualStyleBackColor = False
        '
        'pnlHeader
        '
        Me.pnlHeader.Controls.Add(Me.chkXls)
        Me.pnlHeader.Controls.Add(Me.chkDoc)
        Me.pnlHeader.Controls.Add(Me.btnHeader)
        Me.pnlHeader.Controls.Add(Me.pb1)
        Me.pnlHeader.Controls.Add(Me.pb2)
        Me.pnlHeader.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.pnlHeader.Location = New System.Drawing.Point(703, 321)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(381, 95)
        Me.pnlHeader.TabIndex = 31
        '
        'chkXls
        '
        Me.chkXls.AutoSize = True
        Me.chkXls.Checked = True
        Me.chkXls.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkXls.Location = New System.Drawing.Point(128, 51)
        Me.chkXls.Name = "chkXls"
        Me.chkXls.Size = New System.Drawing.Size(67, 27)
        Me.chkXls.TabIndex = 29
        Me.chkXls.Text = "Excel"
        Me.chkXls.UseVisualStyleBackColor = True
        '
        'chkDoc
        '
        Me.chkDoc.AutoSize = True
        Me.chkDoc.Checked = True
        Me.chkDoc.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDoc.Location = New System.Drawing.Point(128, 18)
        Me.chkDoc.Name = "chkDoc"
        Me.chkDoc.Size = New System.Drawing.Size(67, 27)
        Me.chkDoc.TabIndex = 29
        Me.chkDoc.Text = "Word"
        Me.chkDoc.UseVisualStyleBackColor = True
        '
        'pnlFF
        '
        Me.pnlFF.Controls.Add(Me.gbString)
        Me.pnlFF.Controls.Add(Me.btnRename)
        Me.pnlFF.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.pnlFF.Location = New System.Drawing.Point(703, 426)
        Me.pnlFF.Name = "pnlFF"
        Me.pnlFF.Size = New System.Drawing.Size(381, 85)
        Me.pnlFF.TabIndex = 32
        '
        'gbString
        '
        Me.gbString.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.gbString.Controls.Add(Me.RadioButton2)
        Me.gbString.Controls.Add(Me.RadioButton1)
        Me.gbString.Location = New System.Drawing.Point(182, 19)
        Me.gbString.Name = "gbString"
        Me.gbString.Size = New System.Drawing.Size(172, 59)
        Me.gbString.TabIndex = 30
        Me.gbString.TabStop = False
        Me.gbString.Text = "共通文字列設定"
        Me.gbString.Visible = False
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(99, 24)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(58, 27)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "外す"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(24, 24)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(73, 27)
        Me.RadioButton1.TabIndex = 0
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "つける"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'btnColor
        '
        Me.btnColor.BackColor = System.Drawing.SystemColors.Control
        Me.btnColor.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnColor.Location = New System.Drawing.Point(3, 41)
        Me.btnColor.Name = "btnColor"
        Me.btnColor.Size = New System.Drawing.Size(262, 38)
        Me.btnColor.TabIndex = 33
        Me.btnColor.Text = "作業表　記号番号単位色つけ(DB)"
        Me.btnColor.UseVisualStyleBackColor = False
        '
        'pnlSheet
        '
        Me.pnlSheet.Controls.Add(Me.Label1)
        Me.pnlSheet.Controls.Add(Me.Label2)
        Me.pnlSheet.Controls.Add(Me.numHeaderRow)
        Me.pnlSheet.Controls.Add(Me.numDataRow)
        Me.pnlSheet.Controls.Add(Me.Label3)
        Me.pnlSheet.Controls.Add(Me.lstSheet)
        Me.pnlSheet.Location = New System.Drawing.Point(314, 61)
        Me.pnlSheet.Name = "pnlSheet"
        Me.pnlSheet.Size = New System.Drawing.Size(653, 140)
        Me.pnlSheet.TabIndex = 34
        '
        'pnlSagyohyo
        '
        Me.pnlSagyohyo.Controls.Add(Me.btnCreateSagyo)
        Me.pnlSagyohyo.Controls.Add(Me.btnSagyolistOut)
        Me.pnlSagyohyo.Controls.Add(Me.Label4)
        Me.pnlSagyohyo.Controls.Add(Me.Label5)
        Me.pnlSagyohyo.Controls.Add(Me.btnColor)
        Me.pnlSagyohyo.Controls.Add(Me.cmbItaku)
        Me.pnlSagyohyo.Controls.Add(Me.cmbKaisu)
        Me.pnlSagyohyo.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.pnlSagyohyo.Location = New System.Drawing.Point(314, 569)
        Me.pnlSagyohyo.Name = "pnlSagyohyo"
        Me.pnlSagyohyo.Size = New System.Drawing.Size(499, 125)
        Me.pnlSagyohyo.TabIndex = 35
        '
        'lblDB
        '
        Me.lblDB.BackColor = System.Drawing.SystemColors.Control
        Me.lblDB.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDB.Location = New System.Drawing.Point(669, 514)
        Me.lblDB.Name = "lblDB"
        Me.lblDB.Size = New System.Drawing.Size(489, 39)
        Me.lblDB.TabIndex = 36
        '
        'ss
        '
        Me.ss.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tssl})
        Me.ss.Location = New System.Drawing.Point(0, 789)
        Me.ss.Name = "ss"
        Me.ss.Size = New System.Drawing.Size(1234, 23)
        Me.ss.TabIndex = 37
        Me.ss.Text = "StatusStrip1"
        '
        'tssl
        '
        Me.tssl.Name = "tssl"
        Me.tssl.Size = New System.Drawing.Size(27, 18)
        Me.tssl.Text = "Ver"
        '
        'frmOpe
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Honeydew
        Me.ClientSize = New System.Drawing.Size(1234, 812)
        Me.Controls.Add(Me.ss)
        Me.Controls.Add(Me.lblDB)
        Me.Controls.Add(Me.pnlSagyohyo)
        Me.Controls.Add(Me.pnlSheet)
        Me.Controls.Add(Me.pnlFF)
        Me.Controls.Add(Me.pnlHeader)
        Me.Controls.Add(Me.btnPrt)
        Me.Controls.Add(Me.gb)
        Me.Controls.Add(Me.lblManageFile)
        Me.Controls.Add(Me.btnCountFiles)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.gbItaku)
        Me.Controls.Add(Me.lblRecCnt)
        Me.Controls.Add(Me.lblCnt)
        Me.Controls.Add(Me.btnFolderPlace)
        Me.Controls.Add(Me.lblFolderCreatePath)
        Me.Controls.Add(Me.btnItiran)
        Me.Controls.Add(Me.btnExcelSelect)
        Me.Controls.Add(Me.btnRecipt)
        Me.Controls.Add(Me.btnImport2DB)
        Me.Controls.Add(Me.lblReceiptFilesPath)
        Me.Controls.Add(Me.btnGetData)
        Me.Controls.Add(Me.btnCreateFolder_FileMove)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmOpe"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "協会けんぽ作業"
        Me.gbItaku.ResumeLayout(False)
        Me.gbItaku.PerformLayout()
        Me.gb.ResumeLayout(False)
        Me.gb.PerformLayout()
        CType(Me.numHeaderRow, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numDataRow, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHeader.ResumeLayout(False)
        Me.pnlHeader.PerformLayout()
        Me.pnlFF.ResumeLayout(False)
        Me.gbString.ResumeLayout(False)
        Me.gbString.PerformLayout()
        Me.pnlSheet.ResumeLayout(False)
        Me.pnlSheet.PerformLayout()
        Me.pnlSagyohyo.ResumeLayout(False)
        Me.pnlSagyohyo.PerformLayout()
        Me.ss.ResumeLayout(False)
        Me.ss.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExcelSelect As System.Windows.Forms.Button
    Friend WithEvents lstSheet As System.Windows.Forms.ListBox
    Friend WithEvents btnGetData As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnFolderPlace As System.Windows.Forms.Button
    Friend WithEvents lblFolderCreatePath As System.Windows.Forms.Label
    Friend WithEvents btnCreateFolder_FileMove As System.Windows.Forms.Button
    Friend WithEvents btnRecipt As System.Windows.Forms.Button
    Friend WithEvents lblReceiptFilesPath As System.Windows.Forms.Label
    Friend WithEvents btnImport2DB As System.Windows.Forms.Button
    Friend WithEvents btnItiran As System.Windows.Forms.Button
    Friend WithEvents rb_set As System.Windows.Forms.RadioButton
    Friend WithEvents rb_koban As System.Windows.Forms.RadioButton
    Friend WithEvents lblCnt As System.Windows.Forms.Label
    Friend WithEvents rb_itaku2 As System.Windows.Forms.RadioButton
    Friend WithEvents rb_itaku1 As System.Windows.Forms.RadioButton
    Friend WithEvents gbItaku As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnCountFiles As System.Windows.Forms.Button
    Public WithEvents lblManageFile As System.Windows.Forms.Label
    Friend WithEvents rb_FileCount As System.Windows.Forms.RadioButton
    Friend WithEvents gb As System.Windows.Forms.GroupBox
    Friend WithEvents rb_createSagyo As System.Windows.Forms.RadioButton
    Friend WithEvents btnCreateSagyo As System.Windows.Forms.Button
    Friend WithEvents lblRecCnt As System.Windows.Forms.Label
    Friend WithEvents numHeaderRow As System.Windows.Forms.NumericUpDown
    Friend WithEvents numDataRow As System.Windows.Forms.NumericUpDown
    Friend WithEvents rb_NohinDtUPD As System.Windows.Forms.RadioButton
    Friend WithEvents rb_FileCountSet As System.Windows.Forms.RadioButton
    Friend WithEvents btnPrt As System.Windows.Forms.Button
    Friend WithEvents rb_printkoban As System.Windows.Forms.RadioButton
    Friend WithEvents cmbKaisu As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbItaku As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnSagyolistOut As System.Windows.Forms.Button
    Friend WithEvents btnHeader As System.Windows.Forms.Button
    Friend WithEvents bg1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents pb1 As System.Windows.Forms.ProgressBar
    Friend WithEvents bg2 As System.ComponentModel.BackgroundWorker
    Friend WithEvents pb2 As System.Windows.Forms.ProgressBar
    Friend WithEvents rb_FileFolderName As System.Windows.Forms.RadioButton
    Friend WithEvents rb_Header As System.Windows.Forms.RadioButton
    Friend WithEvents btnRename As System.Windows.Forms.Button
    Friend WithEvents bg3 As System.ComponentModel.BackgroundWorker
    Friend WithEvents bg4 As System.ComponentModel.BackgroundWorker
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents pnlFF As System.Windows.Forms.Panel
    Friend WithEvents chkXls As System.Windows.Forms.CheckBox
    Friend WithEvents chkDoc As System.Windows.Forms.CheckBox
    Friend WithEvents gbString As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents btnColor As System.Windows.Forms.Button
    Friend WithEvents pnlSheet As System.Windows.Forms.Panel
    Friend WithEvents pnlSagyohyo As System.Windows.Forms.Panel
    Friend WithEvents lblDB As System.Windows.Forms.Label
    Friend WithEvents ss As System.Windows.Forms.StatusStrip
    Friend WithEvents tssl As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents rb_Dummy As System.Windows.Forms.RadioButton

End Class
