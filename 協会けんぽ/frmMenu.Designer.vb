﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMenu
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
    'Windows フォーム デザイナを使用して変更できます。  
    'コード エディタを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnImport = New System.Windows.Forms.Button
        Me.btnNohin = New System.Windows.Forms.Button
        Me.btnList = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'btnImport
        '
        Me.btnImport.Location = New System.Drawing.Point(54, 34)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(170, 56)
        Me.btnImport.TabIndex = 0
        Me.btnImport.Text = "管理表取込"
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'btnNohin
        '
        Me.btnNohin.Location = New System.Drawing.Point(54, 158)
        Me.btnNohin.Name = "btnNohin"
        Me.btnNohin.Size = New System.Drawing.Size(170, 56)
        Me.btnNohin.TabIndex = 2
        Me.btnNohin.Text = "納品準備(項番フォルダに移動）"
        Me.btnNohin.UseVisualStyleBackColor = True
        '
        'btnList
        '
        Me.btnList.Location = New System.Drawing.Point(54, 96)
        Me.btnList.Name = "btnList"
        Me.btnList.Size = New System.Drawing.Size(170, 56)
        Me.btnList.TabIndex = 1
        Me.btnList.Text = "管理表見る・更新"
        Me.btnList.UseVisualStyleBackColor = True
        '
        'frmMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.btnNohin)
        Me.Controls.Add(Me.btnList)
        Me.Controls.Add(Me.btnImport)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmMenu"
        Me.Text = "Menu"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents btnNohin As System.Windows.Forms.Button
    Friend WithEvents btnList As System.Windows.Forms.Button
End Class
