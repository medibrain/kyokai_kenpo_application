﻿Imports System.Data.OleDb
Imports System.Runtime.InteropServices.Marshal


Public Class clsExcel
    Dim dt As New DataTable
    Dim strExPath As String = String.Empty
    Public ofd As New OpenFileDialog

    Dim ex As New ForExcel_kk.clsExcel_kk

    Public Function LoadExcelSheet(Optional strPath As String = "") As ArrayList
        strExPath = strPath
        ofd.Filter = "Excel(xlsx,xls)|*.xlsx;*.xls"
        ofd.FilterIndex = 1
        ofd.ShowDialog()
        If ofd.FileName = String.Empty Then Return Nothing

        strExPath = ofd.FileName

        frmOpe.lblManageFile.Text = strExPath

        Return ex.GetSheetNames(strExPath)


    End Function

    Public Function CreateSagyoSheet(ByVal strWSName As String, _
                                    ByVal dt As DataTable, _
                                    ByVal strSavePath As String) As Boolean

        Try
            ex.CreateExcelNoSave()
            ex.OpenWorkBook(strExPath)
            ex.CreateSagyoSheet(strWSName, dt)

            Return True

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False

        Finally
            ex.CloseExcelNoSave()

        End Try
    End Function

    Public Function CreateSetItakuSheet(ByVal strWSName As String, _
                                    ByVal ds As DataSet, _
                                    ByVal strSavePath As String) As Boolean
        Try
            ex.CreateExcelNoSave()
            ex.OpenWorkBook(strExPath)
            ex.CreateSetItakuSheet(strWSName, ds)

            Return True

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False

        Finally
            ex.CloseExcelNoSave()

        End Try
    End Function

    Public Function OpenWorkBook(ByVal strSheetName As String, _
                                 ByVal intHeaderRow As Integer, _
                                 ByVal intDataStartRow As Integer) As DataTable

        If strExPath = String.Empty Then Return Nothing

        ex.CreateExcelNoSave()
        ex.OpenWorkBook(strExPath, strSheetName)

        dt = ex.GetUsedRange(strSheetName, intHeaderRow, intDataStartRow)
        dt.TableName = strSheetName

        For Each dr As DataRow In dt.Rows
            If dr("項番").ToString = String.Empty Then
                dr.Delete()
            End If
        Next
        dt.AcceptChanges()

        ex.CloseExcelNoSave()

        Return dt

    End Function

    Public Sub CloseExcel()
        ex.CloseExcelNoSave()
    End Sub

End Class
