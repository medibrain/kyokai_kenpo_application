﻿Public Class frmMenu
    Public dsSetting As New DataSet
    Public strConnectionString As String = String.Empty


    Private Sub frmMenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IO.File.Exists("setting.xml") Then
            MessageBox.Show("設定ファイルがありません", Application.ProductName, _
                            MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        Else
            '接続文字列をロード
            Dim sr As New IO.StreamReader("setting.xml", System.Text.Encoding.GetEncoding("shift-jis"))
            dsSetting.ReadXml(sr)
            sr.Close()
            strConnectionString = dsSetting.Tables("connection").Rows(0)("string").ToString

        End If
    End Sub

    
    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim frmope As New frmOpe
        frmope.ShowDialog()

    End Sub

    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnList.Click
        Dim frmlist As New frmList
        frmlist.ShowDialog()

    End Sub

    Private Sub btnNohin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNohin.Click
        Dim frmope As New frmOpe
        frmope.ShowDialog()

    End Sub
End Class